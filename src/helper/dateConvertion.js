export function formatDate(inputDate, withDay = true) {
  const daysOfWeek = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const date = new Date(inputDate);
  const dayOfWeek = daysOfWeek[date.getDay()];
  const day = date.getDate();
  const month = date.toLocaleString("default", { month: "long" });
  const year = date.getFullYear();

  return withDay
    ? `${dayOfWeek}, ${day} ${month} ${year}`
    : `${day} ${month} ${year}`;
}
