export const ACCESS_TOKEN = "access_token";
export const SUCCESS_LOGIN = "success_login";
export const UNAUTHORIZED = "unauthorized";
export const SUCCESS_RESET_PASSWORD = "success_reset_password";
export const SUCCESS_LOGOUT = "success_logout";
export const ROLE = "role";
export const SUCCESS_CREATE = "success_create";
export const SUCCESS_UPDATE = "success_update";
export const API_URL = import.meta.env.VITE_API_URL;
