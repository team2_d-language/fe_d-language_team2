import { BrowserRouter } from "react-router-dom";
import { createRoot } from "react-dom/client";
import App from "./App.jsx";
import AuthProvider from "./contexts/authContext.jsx";
import CategoryProvider from "./contexts/CategoryContext.jsx";
import CourseProvider from "./contexts/CourseContext.jsx";

createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <AuthProvider>
      <CourseProvider>
        <CategoryProvider>
          <App />
        </CategoryProvider>
      </CourseProvider>
    </AuthProvider>
  </BrowserRouter>
);
