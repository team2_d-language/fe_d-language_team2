/* eslint-disable react-hooks/exhaustive-deps */
import {
  CssBaseline,
  Typography,
  Box,
  Stack,
  Container,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import TableComponent from "../components/TableComponent";
import Cookies from "js-cookie";
import { UNAUTHORIZED } from "../constants";
import useInvoiceData from "../hooks/useInvoiceData";
import { useEffect } from "react";
import useLogin from "../hooks/useLogin";

const Invoice = () => {
  const { isLogin } = useLogin();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLogin) {
      Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
      navigate("/");
    }
  }, [isLogin]);

  const { dataInvoice } = useInvoiceData();
  const tableHead = [
    "No",
    "No. Invoice",
    "Date",
    "Total Course",
    "Total Price",
    "Action",
  ];
  return (
    <Container maxWidth="xl" sx={{ my: 14 }}>
      <CssBaseline />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={!isLogin}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {isLogin && (
        <>
          <Stack px={4} mb={3} direction={"row"} gap={1}>
            <Link
              to="/"
              style={{
                textDecoration: "none",
                color: "#828282",
                cursor: "pointer",
              }}
            >
              <Typography style={{ fontWeight: "bold" }}>Home</Typography>
            </Link>
            <Typography style={{ fontWeight: "bold" }} color={"#828282"}>
              &gt;
            </Typography>
            <Typography
              style={{
                fontWeight: "bold",
                cursor: "pointer",
                color: "#ea9e1f",
              }}
            >
              Invoice
            </Typography>
          </Stack>

          <Typography px={4} mb={5} fontWeight={"bold"} variant="h6">
            Menu Invoice
          </Typography>

          <Box px={4}>
            <TableComponent dataTable={dataInvoice} tableHead={tableHead} />
          </Box>
        </>
      )}
    </Container>
  );
};

export default Invoice;
