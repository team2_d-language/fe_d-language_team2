/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import CustomButton from "../components/Button";
import {
  Container,
  Grid,
  Typography,
  Stack,
  TextField,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Link } from "react-router-dom";
import PopupAlert from "../components/PopupAlert";
import useLogin from "../hooks/useLogin";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { SUCCESS_LOGOUT } from "../constants";

const Login = () => {
  const {
    email,
    setEmail,
    loading,
    password,
    setPassword,
    loginError,
    setLoginError,
    showAlertLoginFail,
    setShowAlertLoginFail,
    login,
  } = useLogin();
  const [alertMessage, setAlertMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [firstEmail, setFirstEmail] = useState(true);
  const [firstPassword, setFirstPassword] = useState(true);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    if (Cookies.get(SUCCESS_LOGOUT)) {
      setShowAlert(true);
      setAlertMessage("Successfully logout");
      Cookies.remove(SUCCESS_LOGOUT);
    }
  }, []);

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  const validatePassword = () => {
    if (password.length < 8) {
      return "Password must be at least 8 characters";
    } else {
      return "";
    }
  };

  const validateEmail = () => {
    if (
      String(email)
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      return "";
    } else {
      return "Invalid email format";
    }
  };

  const passwordError = validatePassword();
  const emailError = validateEmail();

  return (
    <>
      {loading && (
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      )}
      <Container maxWidth="md">
        <PopupAlert
          message={alertMessage}
          showAlert={showAlert}
          onClose={() => {
            setShowAlert(false);
            setAlertMessage("");
          }}
        />
        <PopupAlert
          message={loginError}
          showAlert={showAlertLoginFail}
          severity="error"
          onClose={() => {
            setShowAlertLoginFail(false);
            setLoginError("");
          }}
        />
        <Grid container spacing={2}>
          <Grid item xs={12} md={8} sx={{ p: 0, mt: 6 }}>
            <Typography
              variant="h5"
              style={{
                color: "#226957",
                fontWeight: 400,
                display: "flex",
                alignItems: "center",
                gap: 8,
              }}
              sx={{ mb: 2, pt: 3 }}
            >
              Welcome Back!
            </Typography>
            <Typography color="#757575" sx={{ mb: 2 }}>
              Please login first.
            </Typography>
          </Grid>
          <Grid item xs={12} md={12}>
            <Stack spacing={3}>
              <TextField
                fullWidth
                id="email"
                label="Email"
                variant="outlined"
                size="small"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                  setFirstEmail(false);
                }}
                sx={{ my: 1 }}
                error={emailError == "" || firstEmail ? false : true}
                helperText={emailError == "" || firstEmail ? "" : emailError}
              />
              <TextField
                fullWidth
                type={showPassword ? "text" : "password"}
                id="password"
                label="Password"
                variant="outlined"
                size="small"
                sx={{ my: 1 }}
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                  setFirstPassword(false);
                }}
                error={passwordError == "" || firstPassword ? false : true}
                helperText={
                  passwordError == "" || firstPassword ? "" : passwordError
                }
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <div style={{ display: "flex", color: "#757575" }}>
                Forgot Password?&nbsp;
                <Link
                  to="/forgot-password"
                  style={{ textDecoration: "none", color: "#2f80ed" }}
                >
                  {" "}
                  Click Here
                </Link>
              </div>
              <div style={{ marginTop: "32px", textAlign: "end" }}>
                {emailError || passwordError ? (
                  <CustomButton
                    label="Login"
                    color="#226957"
                    width="140px"
                    height="38px"
                    disabled
                  />
                ) : (
                  <CustomButton
                    label="Login"
                    color="#226957"
                    width="140px"
                    height="38px"
                    onClick={login}
                  />
                )}
              </div>
              <div style={{ marginTop: "42px", textAlign: "center" }}>
                Dont have account?{" "}
                <Link
                  to="/register"
                  style={{ textDecoration: "none", color: "#2f80ed" }}
                >
                  Sign Up Here
                </Link>
              </div>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Login;
