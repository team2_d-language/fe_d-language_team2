/* eslint-disable react-hooks/exhaustive-deps */
import {
  CssBaseline,
  Typography,
  Box,
  Stack,
  Container,
  Divider,
  ListItemText,
  List,
  useMediaQuery,
  ListItemButton,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import { API_URL } from "../constants";
import useInvoiceData from "../hooks/useInvoiceData";
import { formatDate } from "../helper/dateConvertion";
import nothing from "../assets/nothing.svg";
import useLogin from "../hooks/useLogin";

const MyClass = () => {
  const { isLogin } = useLogin();

  const isSmallScreen = useMediaQuery("(max-width:500px)");
  const { dataInvoice, error } = useInvoiceData();
  const myClassData = dataInvoice.flatMap((invoice) => invoice.detail);

  return (
    <>
      <CssBaseline />
      <Backdrop
        sx={{ color: "#000", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={error !== 200}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {error === 200 && (
        <Container maxWidth="xl" sx={{ my: 14 }}>
          <Backdrop
            sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={!isLogin}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
          {isLogin && (
            <Box px={isSmallScreen ? 1 : 4}>
              <List>
                {myClassData.length === 0 ? (
                  <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
                    <img src={nothing} alt="nothing" height={200} />
                    <Typography
                      variant="h6"
                      mt={4}
                      color={"#b1b1b1"}
                      textAlign="center"
                    >
                      You have no class yet
                    </Typography>
                  </Stack>
                ) : (
                  myClassData.map((data) => {
                    const labelId = `checkbox-list-label-${data.id}`;
                    return (
                      <div key={data.id}>
                        <ListItemButton sx={{ my: 2 }}>
                          <Stack
                            gap={3}
                            direction={isSmallScreen ? "column" : "row"}
                            alignItems={"center"}
                          >
                            <img
                              width={200}
                              src={`${API_URL}${data.image}`}
                              alt={data.courseName}
                            />
                            <Stack>
                              <ListItemText
                                id={labelId}
                                primary={
                                  <Typography
                                    color="#828282"
                                    variant="subtitle1"
                                    mb={-1}
                                  >
                                    {data.language}
                                  </Typography>
                                }
                              />
                              <ListItemText
                                id={labelId}
                                primary={
                                  <Typography
                                    variant="h6"
                                    sx={{ fontWeight: "bold" }}
                                  >
                                    {data.courseName}
                                  </Typography>
                                }
                              />
                              <ListItemText
                                id={labelId}
                                primary={
                                  <Typography variant="h6" color="#ea9e1f">
                                    Schedule : {formatDate(data.schedule)}
                                  </Typography>
                                }
                              />
                            </Stack>
                          </Stack>
                        </ListItemButton>
                        <Divider />
                      </div>
                    );
                  })
                )}
              </List>
            </Box>
          )}
        </Container>
      )}
    </>
  );
};

export default MyClass;
