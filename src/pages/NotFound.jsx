import {
  Box,
  Button,
  CssBaseline,
  Grid,
  Typography,
  styled,
} from "@mui/material";
import { useNavigate } from "react-router-dom";

const HomeButton = styled(Button)({
  marginTop: "20px",
  textTransform: "none",
  fontSize: 20,
  padding: "6px 18px",
  border: "2px solid",
  borderRadius: 50,
  lineHeight: 1.5,
  backgroundColor: "transparent",
  borderColor: "#fff",
  color: "#fff",
  "&:hover": {
    color: "#dc281e",
    backgroundColor: "#fff",
    border: "2px solid",
    borderColor: "#fff",
  },
});

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        background: "linear-gradient(to right, #f00000, #dc281e)",
        minHeight: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CssBaseline />
      <Grid item xs={12} textAlign="center">
        <Typography
          variant="h1"
          sx={{ fontSize: "136px", fontWeight: "bolder", color: "white" }}
        >
          404
        </Typography>
        <Typography variant="h4" sx={{ color: "white", fontWeight: "bold" }}>
          Page Not Found
        </Typography>
        <HomeButton
          onClick={() => navigate("/")}
          variant="outlined"
          disableRipple
        >
          Home
        </HomeButton>
      </Grid>
    </Box>
  );
};

export default NotFound;
