import {
  Box,
  CssBaseline,
  Container,
  Grid,
  Typography,
  Divider,
  Stack,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import CourseList from "../components/CourseList";
import { useParams } from "react-router-dom";
import nothing from "../assets/nothing.svg";
import { API_URL } from "../constants";
import useCategoryData from "../hooks/useCategoryData";

const ListMenuClass = () => {
  const { id } = useParams();
  const { unauthorized, dataLanguage, dataCourse, error } = useCategoryData(id);

  return (
    <>
      <CssBaseline />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={unauthorized}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {!unauthorized && (
        <>
          {error ? (
            <Stack sx={{ alignItems: "center", my: 5, mt: 18 }}>
              <img src={nothing} alt="nothing" height={200} />
              <Typography color={"#b1b1b1"} textAlign={"center"}>
                Nothing to see here
              </Typography>
            </Stack>
          ) : (
            <>
              <Box
                component="img"
                sx={{
                  backgroundImage: `url('${API_URL}${dataLanguage.banner}')`,
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                  width: "100%",
                  height: "100%",
                  minHeight: "60vh",
                  display: "block",
                  objectFit: "cover",
                }}
              />
              <Box
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Box sx={{ backgroundColor: "white", width: 1335 }}>
                  <Container>
                    <Grid container sx={{ mb: "46px" }}>
                      <Grid item lg={12} sx={{ mt: "46px", px: 2 }}>
                        <Typography
                          color={"#000000"}
                          fontWeight={600}
                          fontSize={"24px"}
                          lineHeight={"30px"}
                        >
                          {dataLanguage.categoryName}
                        </Typography>
                        <Typography
                          color={"#333333"}
                          mt={2}
                          fontWeight={400}
                          fontSize={"16px"}
                          lineHeight={"20px"}
                        >
                          {dataLanguage.description}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Container>
                  <Divider />
                </Box>
              </Box>
              <Container sx={{ mt: 8, mb: 20 }}>
                {dataCourse.length === 0 ? (
                  <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
                    <img src={nothing} alt="nothing" height={200} />
                    <Typography textAlign={"center"} color={"#b1b1b1"}>
                      No related courses found for this category
                    </Typography>
                  </Stack>
                ) : (
                  <Box px={2}>
                    <CourseList courseData={dataCourse} />
                  </Box>
                )}
              </Container>
            </>
          )}
        </>
      )}
    </>
  );
};

export default ListMenuClass;
