/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import {
  Container,
  Stack,
  Typography,
  CssBaseline,
  Divider,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import { useParams } from "react-router-dom";
import CourseList from "../components/CourseList";
import CompDetail from "../components/CompDetail";
import { Box } from "@mui/system";
import nothing from "../assets/nothing.svg";
import useCourseDetail from "../hooks/useCourseDetail";

const CourseDetail = () => {
  const { id } = useParams();
  const { unauthorized, detailCourse, relatedCourse, error } =
    useCourseDetail(id);

  return (
    <>
      <CssBaseline />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={unauthorized}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {!unauthorized && (
        <Box sx={{ px: { xs: 1, md: 6 } }}>
          {error ? (
            <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
              <img src={nothing} alt="nothing" height={200} />
              <Typography color={"#b1b1b1"} textAlign={"center"}>
                Data not found
              </Typography>
            </Stack>
          ) : (
            <>
              <CompDetail detailCourse={detailCourse} />
              <Divider />

              <Container maxWidth="xl" sx={{ mt: 8, mb: 20 }}>
                <Typography
                  color="#226957"
                  fontSize={"24px"}
                  fontWeight={600}
                  justifyContent="center"
                  textAlign="center"
                  sx={{ mb: 6 }}
                >
                  Another class for you
                </Typography>
                {relatedCourse.length === 0 ? (
                  <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
                    <img src={nothing} alt="nothing" height={200} />
                    <Typography color={"#b1b1b1"} textAlign={"center"}>
                      No related courses found for this category
                    </Typography>
                  </Stack>
                ) : (
                  <CourseList courseData={relatedCourse} />
                )}
              </Container>
            </>
          )}
        </Box>
      )}
    </>
  );
};

export default CourseDetail;
