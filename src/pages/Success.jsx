/* eslint-disable react-hooks/exhaustive-deps */
import {
  Backdrop,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import successImg from "../assets/successImg.svg";
import homeLogo from "../assets/homeLogo.svg";
import rightArrowLogo from "../assets/rightArrowLogo.svg";
import CustomButton from "../components/Button";
import { Link, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import Cookies from "js-cookie";
import { UNAUTHORIZED } from "../constants";
import useLogin from "../hooks/useLogin";

const Success = () => {
  const { isLogin } = useLogin();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLogin) {
      Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
      navigate("/");
    }
  }, [isLogin]);

  return (
    <Container sx={{ mt: 16, px: 4 }}>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={!isLogin}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {isLogin && (
        <Stack alignItems="center">
          <img src={successImg} alt="Success" height={280} />
          <Stack spacing={1} my={5} textAlign="center">
            <Typography color="#226957" variant="h5">
              Purchase Successfully
            </Typography>
            <Typography variant="subtitle1">
              Thanks for buying a course! See you in the class.
            </Typography>

            <Stack direction="row" spacing={3} justifyContent="space-between">
              <Link to="/" style={{ width: "100%" }}>
                <CustomButton
                  logo={homeLogo}
                  label="Back to Home"
                  color="#ea9e1f"
                  height="40px"
                  width="100%"
                  resize={false}
                />
              </Link>
              <Link to="/invoice" style={{ width: "100%" }}>
                <CustomButton
                  logo={rightArrowLogo}
                  label="Open Invoice"
                  color="#226957"
                  height="40px"
                  width="100%"
                  resize={false}
                />
              </Link>
            </Stack>
          </Stack>
        </Stack>
      )}
    </Container>
  );
};

export default Success;
