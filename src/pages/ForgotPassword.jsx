/* eslint-disable no-unused-vars */
import CustomButton from "../components/Button";
import {
  Container,
  Grid,
  Typography,
  Stack,
  TextField,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { Link } from "react-router-dom";
import PopupAlert from "../components/PopupAlert";
import { API_URL } from "../constants";

const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const [firstEmail, setFirstEmail] = useState(true);
  const [error, setError] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [open, setOpen] = useState(false);
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);

  const validateEmail = () => {
    if (
      String(email)
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      return "";
    } else {
      return "Invalid email format";
    }
  };

  const emailError = validateEmail();

  const confirmHandler = () => {
    if (email === "") {
      setFirstEmail(false);
      return;
    }

    const resetPassword = async () => {
      try {
        setOpen(true);
        await axios.post(`${API_URL}reset-password`, {
          email: email,
        });
        setOpen(false);
        setShowSuccessAlert(true);
      } catch (e) {
        setOpen(false);
        setError(e.response.data.message);
        setShowAlert(true);
      }
    };
    if (!emailError) resetPassword();
  };

  return (
    <>
      <Container maxWidth="md">
        <PopupAlert
          message={error}
          showAlert={showAlert}
          severity="error"
          onClose={() => {
            setShowAlert(false);
            setError("");
          }}
        />
        <PopupAlert
          message={"Please Check Your Email."}
          showAlert={showSuccessAlert}
          severity="success"
          onClose={() => {
            setShowSuccessAlert(false);
          }}
        />
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8} sx={{ p: 0, mt: 6, mb: 3 }}>
            <Typography
              variant="h5"
              style={{
                color: "#323232",
                fontWeight: 0,
                display: "flex",
                alignItems: "center",
                gap: 8,
              }}
              sx={{ mb: 2, pt: 3 }}
            >
              Reset Password
            </Typography>
            <Typography color="#757575" sx={{ mb: 2 }}>
              Please enter your email address.
            </Typography>
          </Grid>
          <Grid item xs={12} md={12}>
            <Stack spacing={3}>
              <TextField
                fullWidth
                id="email"
                label="Email"
                variant="outlined"
                size="small"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                  setFirstEmail(false);
                }}
                sx={{ my: 1 }}
                error={emailError == "" || firstEmail ? false : true}
                helperText={emailError == "" || firstEmail ? "" : emailError}
              />

              <Stack
                spacing={3}
                direction="row"
                justifyContent="flex-end"
                sx={{ marginTop: "32px", marginLeft: "auto" }}
              >
                <Link to="/login">
                  <CustomButton
                    label="Cancel"
                    color="#ea9e1f"
                    width="140px"
                    height="38px"
                  />
                </Link>
                <CustomButton
                  label="Confirm"
                  color="#226957"
                  width="140px"
                  height="38px"
                  onClick={confirmHandler}
                />
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default ForgotPassword;
