/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import {
  CssBaseline,
  Typography,
  Grid,
  Box,
  Stack,
  useMediaQuery,
} from "@mui/material";
import banner from "../assets/image3.png";
import "./style.css";
import twoPeople from "../assets/2people.png";
import CourseList from "../components/CourseList";
import LanguageList from "../components/LanguageList";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import PopupAlert from "../components/PopupAlert";
import { SUCCESS_LOGIN, UNAUTHORIZED } from "../constants";
import useCourseData from "../hooks/useCourseData";
import useLanguageData from "../hooks/useLanguageData";

const Landing = () => {
  const { dataCourse } = useCourseData();
  const { dataLanguage } = useLanguageData();
  const [randomCourse, setRandomCourse] = useState([]);
  const isSmallScreen = useMediaQuery("(max-width:660px)");
  const [showAlert, setShowAlert] = useState(false);
  const [showAlertUnauthorized, setShowAlertUnauthorized] = useState(false);

  function getRandomObjects(array, count) {
    const shuffledArray = array.sort(() => Math.random() - 0.5);
    return shuffledArray.slice(0, count);
  }

  useEffect(() => {
    if (dataCourse.length !== 0) {
      const randomObjects = getRandomObjects(dataCourse, 6);
      setRandomCourse(randomObjects);
    }
  }, [dataCourse]);

  useEffect(() => {
    if (Cookies.get(SUCCESS_LOGIN)) {
      setShowAlert(true);
      Cookies.remove(SUCCESS_LOGIN);
    }
    if (Cookies.get(UNAUTHORIZED)) {
      setShowAlertUnauthorized(true);
      Cookies.remove(UNAUTHORIZED);
    }
  }, []);

  return (
    <>
      <CssBaseline />
      <PopupAlert
        message="You have successfully logged in!"
        showAlert={showAlert}
        onClose={() => {
          setShowAlert(false);
        }}
      />
      <PopupAlert
        message="You need to login first!"
        showAlert={showAlertUnauthorized}
        severity="error"
        onClose={() => {
          setShowAlertUnauthorized(false);
        }}
      />
      <div className="banner">
        <img src={banner} className="logo" alt="logo" />
        <Typography
          variant="h5"
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            color: "white",
            textAlign: "center",
            width: isSmallScreen ? "85%" : "60%",
            lineHeight: 1,
          }}
        >
          <span
            className="title"
            style={{
              fontSize: "36px",
            }}
          >
            Learn different languages to hone your communication skills{" "}
          </span>{" "}
          <br /> <br />
          <span className="subtitle">
            All the languages you are looking for are available here, so what
            are you waiting for and immediately improve your language skills
          </span>
        </Typography>
      </div>
      <Grid container spacing={16} sx={{ px: isSmallScreen ? 4 : 15, py: 6 }}>
        <Grid item xs={12} md={6} lg={4}>
          <Typography
            variant="h3"
            color="#226957"
            textAlign="center"
            fontWeight={700}
          >
            100+
          </Typography>
          <Typography variant="h6" textAlign="center">
            Choose the class you like and get the skills
          </Typography>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Typography
            variant="h3"
            color="#226957"
            textAlign="center"
            fontWeight={700}
          >
            50+
          </Typography>
          <Typography variant="h6" textAlign="center">
            Having teachers who are highly skilled and competent in the language
          </Typography>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Typography
            variant="h3"
            color="#226957"
            textAlign="center"
            fontWeight={700}
          >
            10+
          </Typography>
          <Typography variant="h6" textAlign="center">
            Many alumni become ministry employees because of their excellent
            language skills
          </Typography>
        </Grid>
      </Grid>

      <Typography
        variant="h5"
        color="#226957"
        textAlign="center"
        style={{ fontWeight: "bold" }}
      >
        Recommended Class
      </Typography>

      <Box sx={{ px: isSmallScreen ? 4 : 10, py: 6, mb: 10 }}>
        <CourseList
          courseData={randomCourse}
          setShowAlertUnauthorized={setShowAlertUnauthorized}
        />
      </Box>

      <Box
        sx={{ px: isSmallScreen ? 4 : 10, py: 6, mb: 10 }}
        style={{ backgroundColor: "#ea9e1f" }}
      >
        <Stack direction={{ md: "row" }} spacing={4}>
          <Stack spacing={3}>
            <Typography variant="h5" color="#fff" sx={{ fontWeight: "bold" }}>
              Gets your best benefit
            </Typography>
            <Typography
              variant="subtitle1"
              color="#fff"
              sx={{ textAlign: "justify" }}
            >
              Explore the beauty of language learning with our comprehensive
              courses. Designed for both beginners and those seeking advanced
              proficiency, our lessons cover grammar, vocabulary, and cultural
              expressions. Engage in interactive activities and dynamic lessons
              that make language acquisition enjoyable and rewarding. Join us on
              this linguistic journey and let language be the key to unlocking
              new opportunities. Enroll in our language courses today to broaden
              your horizons and experience the satisfaction of effective
              language acquisition.
            </Typography>
          </Stack>
          <img
            src={twoPeople}
            alt="2people"
            style={{ width: "100%", maxWidth: "300px", alignSelf: "center" }}
          />
        </Stack>
      </Box>

      <Typography
        variant="h5"
        color="#226957"
        textAlign="center"
        mx={2}
        style={{ fontWeight: "bold" }}
      >
        Available Language Course
      </Typography>

      <LanguageList
        dataLanguage={dataLanguage}
        setShowAlertUnauthorized={setShowAlertUnauthorized}
      />
    </>
  );
};

export default Landing;
