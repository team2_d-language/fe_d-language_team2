/* eslint-disable no-unused-vars */
import {
  AppBar,
  Box,
  Container,
  Stack,
  Toolbar,
  CircularProgress,
  Typography,
  CssBaseline,
} from "@mui/material";
import { Link, useParams } from "react-router-dom";
import logo from "../assets/logo.png";
import CustomButton from "../components/Button";
import successImg from "../assets/successImg.svg";
import sww from "../assets/something-went-wrong.svg";
import useEmailVerification from "../hooks/useEmailVerification";

const ConfirmedSucces = () => {
  const { token } = useParams();
  const { isValid, error } = useEmailVerification(token);

  return (
    <>
      <CssBaseline />
      <Box sx={{ flexGrow: 12, mb: 6 }}>
        <AppBar elevation={0} sx={{ px: 3, py: 2, backgroundColor: "#ffffff" }}>
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <Link to="/" style={{ textDecoration: "none" }}>
              <Typography
                variant="h6"
                component="div"
                sx={{
                  display: "flex",
                  alignItems: "center",
                  fontFamily: "montserrat, sans-serif",
                  color: "#000",
                }}
              >
                <img
                  src={logo}
                  className="logo"
                  alt="logo"
                  style={{ marginRight: "8px" }}
                />
                Language
              </Typography>
            </Link>
          </Toolbar>
        </AppBar>
      </Box>
      <Container>
        {isValid ? (
          <Stack alignItems="center" marginTop={15}>
            <img src={successImg} alt={"trash"} />
            <Stack spacing={1} my={5} textAlign="center">
              <Typography color="#226957" variant="h5">
                Email Confirmation Succes
              </Typography>
              <Typography variant="subtitle1" color={"#4F4F4F"}>
                Thanks for confirmation your email. Please login first
              </Typography>
            </Stack>
            <Stack alignItems="center">
              <Link to="/login">
                <CustomButton
                  // logo={homeLogo}
                  label="Login Here"
                  color="#226957"
                  height="40px"
                  width="160px"
                />
              </Link>
            </Stack>
          </Stack>
        ) : error ? (
          <Stack
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              my: 22,
            }}
          >
            <img src={sww} alt="ssw" height={280} />
            <Typography fontWeight={"bold"} fontSize={20}>
              {error}
            </Typography>
          </Stack>
        ) : (
          <Stack
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              mt: 24,
            }}
          >
            <CircularProgress />
            <Typography mt={2}>Verifying</Typography>
          </Stack>
        )}
      </Container>
    </>
  );
};

export default ConfirmedSucces;
