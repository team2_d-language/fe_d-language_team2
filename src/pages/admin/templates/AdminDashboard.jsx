/* eslint-disable react/prop-types */
import { Navigate } from "react-router-dom";
import ResponsiveDrawer from "../components/ResponsiveDrawer";
import { Backdrop, CircularProgress } from "@mui/material";
import useAllCategories from "../../../hooks/admin/useAllCategories";
import useLogin from "../../../hooks/useLogin";

const AdminDashboard = () => {
  const { role, token } = useLogin();
  const { forbidden } = useAllCategories();
  if (role !== "admin" || !token) {
    return <Navigate to="/" />;
  }

  return (
    <>
      <Backdrop
        sx={{ color: "#000", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={forbidden}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {!forbidden && <ResponsiveDrawer />}
    </>
  );
};

export default AdminDashboard;
