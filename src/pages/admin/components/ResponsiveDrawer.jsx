import logo from "../../../assets/logo.png";
import MenuIcon from "@mui/icons-material/Menu";
import CategoryIcon from "@mui/icons-material/Category";
import PeopleIcon from "@mui/icons-material/People";
import LogoutIcon from "@mui/icons-material/Logout";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import LocalAtmIcon from "@mui/icons-material/LocalAtm";
import ReceiptIcon from "@mui/icons-material/Receipt";
import { useState } from "react";
import { Link, Outlet, useLocation } from "react-router-dom";
import {
  AppBar,
  Box,
  Button,
  CssBaseline,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from "@mui/material";
import useAllCategories from "../../../hooks/admin/useAllCategories";
import { API_URL } from "../../../constants";
import "../../style.css";
import useLogin from "../../../hooks/useLogin";
const drawerWidth = 240;

const drawerMenu = [
  {
    name: "Categories",
    icon: CategoryIcon,
    link: "category",
  },
  {
    name: "Users",
    icon: PeopleIcon,
    link: "user",
  },
  {
    name: "Payments",
    icon: LocalAtmIcon,
    link: "payment",
  },
  {
    name: "Invoices",
    icon: ReceiptIcon,
    link: "invoice",
  },
];

function ResponsiveDrawer() {
  const { dataLanguage } = useAllCategories();
  const [mobileOpen, setMobileOpen] = useState(false);
  const { handleLogout } = useLogin();
  const [openSubMenu, setOpenSubMenu] = useState(false);
  const location = useLocation();
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div
      style={{
        backgroundColor: "#232f47",
        height: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Toolbar sx={{ backgroundColor: "white" }}>
        <Link to="/" style={{ textDecoration: "none" }}>
          <Typography
            variant="h6"
            component="div"
            sx={{
              display: "flex",
              alignItems: "center",
              fontFamily: "montserrat, sans-serif",
              color: "#000",
            }}
          >
            <img src={logo} className="logo" alt="logo" />
            Language
          </Typography>
        </Link>
      </Toolbar>
      <List sx={{ px: 1, overflowY: "auto" }} className="list">
        {drawerMenu.map((menu, index) => (
          <div key={index}>
            <Link
              to={menu.link}
              style={{
                textDecoration: "none",
                color:
                  location.pathname === `/admin/${menu.link}`
                    ? "#232f47"
                    : "white",
              }}
              onClick={handleDrawerToggle}
            >
              <ListItem
                disablePadding
                sx={{
                  borderRadius: 3,
                  backgroundColor:
                    location.pathname === `/admin/${menu.link}`
                      ? "white"
                      : "transparent",
                }}
              >
                <ListItemButton
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <ListItemIcon
                    style={{
                      color:
                        location.pathname === `/admin/${menu.link}`
                          ? "#232f47"
                          : "white",
                      minWidth: "36px",
                    }}
                  >
                    <menu.icon />
                  </ListItemIcon>
                  <ListItemText primary={menu.name} />
                  {menu.name === "Categories" && (
                    <Button
                      onClick={(event) => {
                        event.preventDefault();
                        event.stopPropagation();
                        setOpenSubMenu((prev) => !prev);
                      }}
                      disableRipple
                      style={{
                        color:
                          location.pathname === `/admin/${menu.link}`
                            ? "#232f47"
                            : "white",
                      }}
                      className="buttonDropdownSidebar"
                    >
                      {openSubMenu ? <ExpandMoreIcon /> : <ChevronRightIcon />}
                    </Button>
                  )}
                </ListItemButton>
              </ListItem>
            </Link>
            {menu.name === "Categories" &&
              openSubMenu &&
              dataLanguage.map((data) => (
                <Link
                  key={data.id}
                  to={`course/${data.categoryName}`}
                  onClick={handleDrawerToggle}
                  style={{
                    textDecoration: "none",
                    color:
                      location.pathname === `/admin/course/${data.categoryName}`
                        ? "#232f47"
                        : "white",
                  }}
                >
                  <ListItem
                    disablePadding
                    sx={{
                      textDecoration: "none",
                      borderRadius: 3,
                      backgroundColor:
                        location.pathname ===
                        `/admin/course/${data.categoryName}`
                          ? "white"
                          : "transparent",
                      width: "85%",
                      float: "right",
                    }}
                  >
                    <ListItemButton sx={{ height: "36px" }}>
                      <ListItemIcon
                        style={{
                          color:
                            location.pathname ===
                            `/admin/course/${data.categoryName}`
                              ? "#232f47"
                              : "white",
                          minWidth: "36px",
                        }}
                      >
                        {/* <ClassIcon /> */}
                        <img src={`${API_URL}${data.flag}`} width={"22px"} />
                      </ListItemIcon>
                      <ListItemText primary={data.categoryName} />
                    </ListItemButton>
                  </ListItem>
                </Link>
              ))}
          </div>
        ))}
      </List>
    </div>
  );

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
          backgroundColor: "#00abad",
          // backgroundColor: "#fff",
          boxShadow: "none",
        }}
      >
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            {/* <Typography variant="h6" noWrap component="div" color={"black"}> */}
            Dashboard
          </Typography>
          <div
            style={{
              display: "flex",
              backgroundColor: "#eb5757",
              padding: 2,
              borderRadius: 5,
            }}
            onClick={handleLogout}
          >
            <LogoutIcon style={{ cursor: "pointer" }} />
          </div>
        </Toolbar>
      </AppBar>
      <Divider />
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
      >
        <Drawer
          // container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          overflow: "auto",
          height: "100vh",
        }}
      >
        <Toolbar />
        <Outlet />
      </Box>
    </Box>
  );
}

export default ResponsiveDrawer;
