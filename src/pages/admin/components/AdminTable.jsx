/* eslint-disable react/prop-types */
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import CustomButton from "../../../components/Button";
import { Typography } from "@mui/material";
import { API_URL } from "../../../constants";
import { formatDate } from "../../../helper/dateConvertion";
import { currencyConvertion } from "../../../helper/currencyConvertion";

export default function AdminTable({ data, tableHead, type, category }) {
  return (
    <TableContainer component={Paper} style={{ marginTop: "46px" }}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead sx={{ background: "#090d14" }}>
          <TableRow>
            {tableHead.map((headLabel, i) => (
              <TableCell key={i} className="tableText" sx={{ color: "#fff" }}>
                {headLabel}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {!data || data.length === 0 ? (
            <TableRow>
              <TableCell colSpan={6} align="center">
                No data available
              </TableCell>
            </TableRow>
          ) : type === "category" ? (
            data.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.categoryName}</TableCell>
                <TableCell className="tableText">
                  <img src={`${API_URL}${row.flag}`} width={"180px"} />
                </TableCell>
                <TableCell className="tableText">{row.description}</TableCell>
                <TableCell className="tableText">
                  {row.isActive ? (
                    <Typography color={"#25d366"}>Active</Typography>
                  ) : (
                    <Typography color={"#d80403"}>Inactive</Typography>
                  )}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`/admin/category/update/${row.id}`}>
                    <CustomButton color="#ea9e1f" width="80%" label="Update" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : type === "course" ? (
            data.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.name}</TableCell>
                <TableCell className="tableText">
                  <img src={`${API_URL}${row.image}`} width={"180px"} />
                </TableCell>
                <TableCell className="tableText">{row.price}</TableCell>
                <TableCell className="tableText">{row.description}</TableCell>
                <TableCell className="tableText">
                  {row.isActive ? (
                    <Typography color={"#25d366"}>Active</Typography>
                  ) : (
                    <Typography color={"#d80403"}>Inactive</Typography>
                  )}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`/admin/course/${category}/${row.id}/update`}>
                    {/* <Link> */}
                    <CustomButton color="#ea9e1f" width="80%" label="Update" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : type === "user" ? (
            data.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.fullName}</TableCell>
                <TableCell className="tableText">{row.email}</TableCell>
                <TableCell className="tableText">{row.role}</TableCell>
                <TableCell className="tableText">
                  {row.isActive ? (
                    <Typography color={"#25d366"}>Active</Typography>
                  ) : (
                    <Typography color={"#d80403"}>Inactive</Typography>
                  )}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`update/${row.id}`}>
                    <CustomButton color="#ea9e1f" width="80%" label="Update" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : type === "payment" ? (
            data.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.paymentMethod}</TableCell>
                <TableCell className="tableText">
                  <img src={`${API_URL}${row.logo}`} width={"40px"} />
                </TableCell>
                <TableCell className="tableText">
                  {row.isActive ? (
                    <Typography color={"#25d366"}>Active</Typography>
                  ) : (
                    <Typography color={"#d80403"}>Inactive</Typography>
                  )}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`update/${row.id}`}>
                    <CustomButton color="#ea9e1f" width="50%" label="Update" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : type === "invoice" ? (
            data.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {row.invoiceNumber}
                </TableCell>
                <TableCell className="tableText">{row.user.email}</TableCell>
                <TableCell className="tableText">
                  {formatDate(row.createdAt)}
                </TableCell>
                <TableCell className="tableText">{row.totalCourse}</TableCell>
                <TableCell className="tableText">
                  IDR {currencyConvertion(row.totalPrice)}
                </TableCell>
                <TableCell className="tableText">
                  {row.payment.paymentMethod}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`${row.id}`}>
                    <CustomButton color="#ea9e1f" width="80%" label="Detail" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={6} align="center">
                No data available
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

AdminTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  type: PropTypes.string,
  category: PropTypes.string,
};
