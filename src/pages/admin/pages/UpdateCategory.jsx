/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Box,
  Stack,
  Switch,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_UPDATE } from "../../../constants";
import { useNavigate, useParams } from "react-router-dom";
import PopupAlert from "../../../components/PopupAlert";
import nothing from "../../../assets/nothing.svg";
import Cookies from "js-cookie";
import CustomButton from "../../../components/Button";
import useLogin from "../../../hooks/useLogin";
import useLanguageData from "../../../hooks/useLanguageData";
import useAllCategories from "../../../hooks/admin/useAllCategories";

const UpdateCategory = () => {
  const { id } = useParams();
  const { token } = useLogin();
  const { setDataLanguage } = useLanguageData();
  const { setDataLanguage: setDataLanguageAdmin } = useAllCategories();
  const navigate = useNavigate();
  const [code, setCode] = useState(200);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [bannerImage, setBannerImage] = useState(null);
  const [flagImage, setFlagImage] = useState(null);
  const [oldBanner, setOldBanner] = useState(null);
  const [oldFlag, setOldFlag] = useState(null);
  const [description, setDescription] = useState("");
  const [isActive, setIsActive] = useState(true);
  const [errors, setErrors] = useState({
    categoryName: false,
    bannerImage: false,
    flagImage: false,
    description: false,
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const responseLanguage = await axios.get(`${API_URL}categories/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setCategoryName(responseLanguage.data.categoryName || "");
        setDescription(responseLanguage.data.description || "");
        if (responseLanguage.data.banner) {
          setOldBanner(`${API_URL}${responseLanguage.data.banner}`);
        }
        if (responseLanguage.data.flag) {
          setOldFlag(`${API_URL}${responseLanguage.data.flag}`);
        }
        setIsActive(responseLanguage.data.isActive);
        setCode(200);
      } catch (error) {
        if (error.response.status === 404) {
          setCode(404);
          setAlertMessage("Category Not Found");
          setShowAlert(true);
        }
        if (error.response.status === 403 || error.response.status === 401)
          navigate("/");
      }
    };
    fetchData();
  }, [id, token]);

  const handleIsActiveChange = () => {
    setIsActive(!isActive);
  };

  const handleCategoryNameChange = (event) => {
    setCategoryName(event.target.value);
  };

  const handleBannerImageChange = (event) => {
    const file = event.target.files[0];
    setBannerImage(file || null);
  };

  const handleFlagImageChange = (event) => {
    const file = event.target.files[0];
    setFlagImage(file || null);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newErrors = {
      categoryName: !categoryName,
      description: !description,
    };

    if (bannerImage) {
      newErrors.bannerImage = false;
    }

    if (flagImage) {
      newErrors.flagImage = false;
    }

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      const formData = new FormData();
      formData.append("CategoryName", categoryName);
      formData.append("Banner", bannerImage);
      formData.append("Flag", flagImage);
      formData.append("Description", description);
      formData.append("IsActive", isActive);

      await axios.put(`${API_URL}categories/${id}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data",
        },
      });
      Cookies.set(SUCCESS_UPDATE, 1, { expires: 1 });

      setCategoryName("");
      setBannerImage(null);
      setOldBanner(null);
      setFlagImage(null);
      setOldFlag(null);
      setDescription("");
      const responseActive = await axios.get(`${API_URL}categories/active`);
      setDataLanguage(responseActive.data.categories);

      const response = await axios.get(`${API_URL}categories`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setDataLanguageAdmin(response.data);
      navigate("/admin/category");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        severity={"error"}
        onClose={() => {
          setShowAlert(false);
          setAlertMessage("");
        }}
      />
      {code === 404 ? (
        <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
          <img src={nothing} alt="nothing" height={200} />
          <Typography textAlign={"center"}>Data not found</Typography>
        </Stack>
      ) : (
        <Grid container justifyContent="center" gap={2}>
          <Grid item xs={12} md={8} lg={6}>
            <CustomButton
              label="Back"
              float={"left"}
              color="#d80403"
              width="180px"
              onClick={() => navigate(`/admin/category`)}
            />
          </Grid>
          <Grid item xs={12} sm={10} md={8} lg={6}>
            <Paper elevation={3} style={{ padding: 16 }}>
              <Typography variant="h5" gutterBottom>
                Update Category
              </Typography>
              <form onSubmit={handleSubmit}>
                <TextField
                  label="Category Name"
                  variant="outlined"
                  fullWidth
                  margin="normal"
                  value={categoryName}
                  onChange={handleCategoryNameChange}
                  error={errors.categoryName}
                  helperText={
                    errors.categoryName && "Category Name is required"
                  }
                />
                <Box mt={2}>
                  <InputLabel
                    htmlFor="banner-image"
                    style={{ marginBottom: 8 }}
                  >
                    Banner
                  </InputLabel>
                  <img
                    src={
                      bannerImage ? URL.createObjectURL(bannerImage) : oldBanner
                    }
                    alt="Banner"
                    style={{
                      maxWidth: "100%",
                      maxHeight: "200px",
                      objectFit: "cover",
                    }}
                  />
                  <FormControl fullWidth>
                    <Input
                      type="file"
                      id="banner-image"
                      accept="image/*"
                      onChange={handleBannerImageChange}
                      error={errors.bannerImage}
                    />
                    {errors.bannerImage && (
                      <Typography color="error">Banner is required</Typography>
                    )}
                  </FormControl>
                </Box>
                <Box mt={2}>
                  <InputLabel htmlFor="flag-image" style={{ marginBottom: 8 }}>
                    Flag
                  </InputLabel>
                  <img
                    src={flagImage ? URL.createObjectURL(flagImage) : oldFlag}
                    alt="Flag"
                    style={{
                      maxWidth: "100%",
                      maxHeight: "200px",
                      objectFit: "cover",
                    }}
                  />
                  <FormControl fullWidth>
                    <Input
                      type="file"
                      id="flag-image"
                      accept="image/*"
                      onChange={handleFlagImageChange}
                      error={errors.flagImage}
                    />
                    {errors.flagImage && (
                      <Typography color="error">Flag is required</Typography>
                    )}
                  </FormControl>
                </Box>
                <Box mt={2}>
                  <Typography variant="body1" gutterBottom>
                    Is Active:
                  </Typography>
                  <Switch
                    checked={isActive}
                    onChange={handleIsActiveChange}
                    inputProps={{ "aria-label": "is active checkbox" }}
                  />
                </Box>
                <TextField
                  label="Description"
                  variant="outlined"
                  fullWidth
                  multiline
                  rows={4}
                  margin="normal"
                  value={description}
                  onChange={handleDescriptionChange}
                  error={errors.description}
                  helperText={errors.description && "Description is required"}
                />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  style={{ marginTop: 16 }}
                >
                  Submit
                </Button>
              </form>
            </Paper>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default UpdateCategory;
