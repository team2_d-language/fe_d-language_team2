/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Box,
  Stack,
  Switch,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_UPDATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate, useParams } from "react-router-dom";
import CustomButton from "../../../components/Button";
import PopupAlert from "../../../components/PopupAlert";
import nothing from "../../../assets/nothing.svg";
import useLogin from "../../../hooks/useLogin";

const UpdatePayment = () => {
  const { token } = useLogin();
  const { id } = useParams();
  const navigate = useNavigate();
  const [paymentMethod, setPaymentMethod] = useState("");
  const [logo, setLogo] = useState(null);
  const [isActive, setIsActive] = useState(true);
  const [oldLogo, setOldLogo] = useState(null);
  const [errors, setErrors] = useState({
    paymentMethod: false,
    logo: false,
  });

  const [code, setCode] = useState(200);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get(`${API_URL}payments/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const data = response.data;
        setPaymentMethod(data.paymentMethod);
        setOldLogo(`${API_URL}${data.logo}`);
        setIsActive(data.isActive);
        setCode(200);
      } catch (error) {
        if (error.response.status === 404) {
          setCode(404);
          setAlertMessage("Payment Not Found");
          setShowAlert(true);
        }
        if (error.response.status === 403 || error.response.status === 401)
          navigate("/");
      }
    };
    getData();
  }, [id, token]);

  const handleIsActiveChange = () => {
    setIsActive(!isActive);
  };

  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value);
  };

  const handleLogoChange = (event) => {
    const file = event.target.files[0];
    setLogo(file);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const newErrors = {
      paymentMethod: !paymentMethod,
    };

    if (logo) {
      newErrors.logo = false;
    }

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      const formData = new FormData();
      formData.append("PaymentMethod", paymentMethod);
      formData.append("Logo", logo);
      formData.append("IsActive", isActive);

      await axios.put(`${API_URL}payments/${id}`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data",
        },
      });
      Cookies.set(SUCCESS_UPDATE, 1, { expires: 1 });

      setPaymentMethod("");
      setLogo(null);
      navigate(`/admin/payment/`);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        severity={"error"}
        onClose={() => {
          setShowAlert(false);
          setAlertMessage("");
        }}
      />
      {code === 404 ? (
        <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
          <img src={nothing} alt="nothing" height={200} />
          <Typography textAlign={"center"}>Data not found</Typography>
        </Stack>
      ) : (
        <Grid container justifyContent="center" gap={2}>
          <Grid item xs={12} md={8} lg={6}>
            <CustomButton
              label="Back"
              float={"left"}
              color="#d80403"
              width="180px"
              onClick={() => navigate("/admin/payment")}
            />
          </Grid>
          <Grid item xs={12} md={8} lg={6}>
            <Paper elevation={3} style={{ padding: 16 }}>
              <Typography variant="h5" gutterBottom>
                Update Payment Method
              </Typography>
              <form onSubmit={handleSubmit}>
                <TextField
                  label="Payment Name"
                  variant="outlined"
                  fullWidth
                  margin="normal"
                  value={paymentMethod}
                  onChange={handlePaymentMethodChange}
                  error={errors.paymentMethod}
                  helperText={
                    errors.paymentMethod && "Payment Name is required"
                  }
                />
                <Box mt={2}>
                  <InputLabel htmlFor="image" style={{ marginBottom: 8 }}>
                    Image
                  </InputLabel>
                  <img
                    src={logo ? URL.createObjectURL(logo) : oldLogo}
                    alt="Flag"
                    style={{
                      maxWidth: "100%",
                      maxHeight: "200px",
                      objectFit: "cover",
                    }}
                  />
                  <FormControl fullWidth>
                    <Input
                      type="file"
                      id="image"
                      accept="image/*"
                      onChange={handleLogoChange}
                      error={errors.image}
                    />
                    {errors.image && (
                      <Typography color="error">Image is required</Typography>
                    )}
                  </FormControl>
                </Box>
                <Box mt={2}>
                  <Typography variant="body1" gutterBottom>
                    Is Active:
                  </Typography>
                  <Switch
                    checked={isActive}
                    onChange={handleIsActiveChange}
                    inputProps={{ "aria-label": "is active checkbox" }}
                  />
                </Box>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  style={{ marginTop: 16 }}
                >
                  Submit
                </Button>
              </form>
            </Paper>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default UpdatePayment;
