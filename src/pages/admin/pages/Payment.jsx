import { useNavigate, useParams } from "react-router-dom";
import AdminTable from "../components/AdminTable";
import PopupAlert from "../../../components/PopupAlert";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { SUCCESS_CREATE, SUCCESS_UPDATE } from "../../../constants";
import CustomButton from "../../../components/Button";
import useAllPayments from "../../../hooks/admin/useAllPayments";
import { Typography } from "@mui/material";

const Payment = () => {
  const navigate = useNavigate();
  const { category } = useParams();
  const dataPayments = useAllPayments(category);
  const tableHead = ["No", "Name", "Logo", "Status", "Action"];
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const handleAddPage = () => {
    navigate("add");
  };

  useEffect(() => {
    if (Cookies.get(SUCCESS_CREATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Create Payment");
      Cookies.remove(SUCCESS_CREATE);
    }
    if (Cookies.get(SUCCESS_UPDATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Update Payment");
      Cookies.remove(SUCCESS_UPDATE);
    }
  }, []);

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        onClose={() => {
          setShowAlert(false);
        }}
      />
      <CustomButton
        label="Add Payment"
        float={"right"}
        color="#66b423"
        width="180px"
        onClick={handleAddPage}
      />
      <Typography mt={6} variant="h5" textAlign={"center"}>
        Payments
      </Typography>
      <AdminTable
        data={dataPayments}
        tableHead={tableHead}
        type={"payment"}
        category={category}
      />
    </>
  );
};

export default Payment;
