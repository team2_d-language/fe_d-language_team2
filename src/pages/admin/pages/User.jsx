import { useEffect, useState } from "react";
import useAllUsers from "../../../hooks/admin/useAllUser";
import AdminTable from "../components/AdminTable";
import Cookies from "js-cookie";
import { SUCCESS_CREATE, SUCCESS_UPDATE } from "../../../constants";
import PopupAlert from "../../../components/PopupAlert";
import { useNavigate } from "react-router-dom";
import CustomButton from "../../../components/Button";
import { Typography } from "@mui/material";

const User = () => {
  const navigate = useNavigate();
  const dataUser = useAllUsers();
  const tableHead = ["No", "Name", "Email", "Role", "Status", "Action"];
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const handleAddPage = () => {
    navigate("add");
  };

  useEffect(() => {
    if (Cookies.get(SUCCESS_CREATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Create User");
      Cookies.remove(SUCCESS_CREATE);
    }
    if (Cookies.get(SUCCESS_UPDATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Update User");
      Cookies.remove(SUCCESS_UPDATE);
    }
  }, []);

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        onClose={() => {
          setShowAlert(false);
        }}
      />
      <CustomButton
        label="Add User"
        float={"right"}
        color="#66b423"
        width="180px"
        onClick={handleAddPage}
      />
      <Typography mt={6} variant="h5" textAlign={"center"}>
        Users
      </Typography>
      <AdminTable data={dataUser} tableHead={tableHead} type={"user"} />
    </>
  );
};

export default User;
