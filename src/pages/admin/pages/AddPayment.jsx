/* eslint-disable react-hooks/exhaustive-deps */
import { useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Box,
  Switch,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_CREATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import CustomButton from "../../../components/Button";
import useLogin from "../../../hooks/useLogin";

const AddCourse = () => {
  const { token } = useLogin();
  const navigate = useNavigate();
  const [paymentMethod, setPaymentMethod] = useState("");
  const [logo, setLogo] = useState(null);
  const [isActive, setIsActive] = useState(true);
  const [errors, setErrors] = useState({
    paymentMethod: false,
    logo: false,
  });

  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value);
  };

  const handleLogoChange = (event) => {
    const file = event.target.files[0];
    setLogo(file);
  };

  const handleIsActiveChange = () => {
    setIsActive(!isActive);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const newErrors = {
      paymentMethod: !paymentMethod,
      logo: !logo,
    };

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      await axios.post(
        `${API_URL}payments`,
        {
          PaymentMethod: paymentMethod,
          Logo: logo,
          IsActive: isActive,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );

      Cookies.set(SUCCESS_CREATE, 1, { expires: 1 });
      setPaymentMethod("");
      setLogo(null);
      navigate("/admin/payment");
    } catch (e) {
      if (e.response.status == 403 || e.response.status == 401) navigate("/");
    }
  };

  return (
    <>
      <Grid container justifyContent="center" gap={2}>
        <Grid item xs={12} md={8} lg={6}>
          <CustomButton
            label="Back"
            float={"left"}
            color="#d80403"
            width="180px"
            onClick={() => navigate("/admin/payment")}
          />
        </Grid>
        <Grid item xs={12} md={8} lg={6}>
          <Paper elevation={3} style={{ padding: 16 }}>
            <Typography variant="h5" gutterBottom>
              Add Payment
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                label="Payment Method"
                variant="outlined"
                fullWidth
                margin="normal"
                value={paymentMethod}
                onChange={handlePaymentMethodChange}
                error={errors.paymentMethod}
                helperText={
                  errors.paymentMethod && "Payment Method is required"
                }
              />
              <Box mt={2}>
                <InputLabel htmlFor="logo" style={{ marginBottom: 8 }}>
                  Logo
                </InputLabel>
                <FormControl fullWidth>
                  <Input
                    type="file"
                    id="logo"
                    accept="image/*"
                    onChange={handleLogoChange}
                    error={errors.logo}
                  />
                  {errors.logo && (
                    <Typography color="error">Logo is required</Typography>
                  )}
                </FormControl>
              </Box>
              <Box mt={2}>
                <Typography variant="body1" gutterBottom>
                  Is Active:
                </Typography>
                <Switch
                  checked={isActive}
                  onChange={handleIsActiveChange}
                  inputProps={{ "aria-label": "is active checkbox" }}
                />
              </Box>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 16 }}
              >
                Submit
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddCourse;
