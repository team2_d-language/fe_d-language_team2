/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Box,
  Select,
  MenuItem,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_CREATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate, useParams } from "react-router-dom";
import CustomButton from "../../../components/Button";
import useAllCategories from "../../../hooks/admin/useAllCategories";
import useLogin from "../../../hooks/useLogin";
import useCourseData from "../../../hooks/useCourseData";

const AddCourse = () => {
  const { token } = useLogin();
  const { category } = useParams();
  const navigate = useNavigate();
  const { setDataCourse } = useCourseData();
  const { dataLanguage } = useAllCategories();
  const defaultCategoryId = (() => {
    const defaultCategoryName = category;
    const defaultCategory = dataLanguage.find(
      (data) => data.categoryName === defaultCategoryName
    );
    return defaultCategory ? defaultCategory.id : "";
  })();
  const [courseName, setCourseName] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(null);
  const [errors, setErrors] = useState({
    courseName: false,
    categoryId: false,
    price: false,
    description: false,
    image: false,
  });

  useEffect(() => {
    if (dataLanguage) setCategoryId(defaultCategoryId);
  }, [dataLanguage]);

  const handleCourseNameChange = (event) => {
    setCourseName(event.target.value);
  };

  const handleCategoryIdChange = (event) => {
    setCategoryId(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    setImage(file);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const newErrors = {
      courseName: !courseName,
      categoryId: !categoryId,
      description: !description,
      price: price === "" || isNaN(price) || price <= 0,
      image: !image,
    };

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      await axios.post(
        `${API_URL}courses`,
        {
          Name: courseName,
          Category_id: categoryId,
          Description: description,
          Price: price,
          Image: image,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );

      Cookies.set(SUCCESS_CREATE, 1, { expires: 1 });
      setCourseName("");
      setCategoryId("");
      setPrice(0);
      setDescription("");
      setImage(null);

      const response = await axios.get(`${API_URL}courses/active`);
      setDataCourse(response.data.courses);

      navigate(`/admin/course/${category}`);
    } catch (e) {
      if (e.response.status == 403 || e.response.status == 401) navigate("/");
    }
  };

  return (
    <>
      <Grid container justifyContent="center" gap={2}>
        <Grid item xs={12} md={8} lg={6}>
          <CustomButton
            label="Back"
            float={"left"}
            color="#d80403"
            width="180px"
            onClick={() => navigate(`/admin/course/${category}`)}
          />
        </Grid>
        <Grid item xs={12} md={8} lg={6}>
          <Paper elevation={3} style={{ padding: 16 }}>
            <Typography variant="h5" gutterBottom>
              Add Course
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                label="Course Name"
                variant="outlined"
                fullWidth
                margin="normal"
                value={courseName}
                onChange={handleCourseNameChange}
                error={errors.courseName}
                helperText={errors.courseName && "Course Name is required"}
              />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={categoryId}
                  label="Category"
                  onChange={handleCategoryIdChange}
                >
                  {dataLanguage.map((data) => (
                    <MenuItem key={data.id} value={data.id}>
                      {data.categoryName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                label="Description"
                variant="outlined"
                fullWidth
                multiline
                rows={4}
                margin="normal"
                value={description}
                onChange={handleDescriptionChange}
                error={errors.description}
                helperText={errors.description && "Description is required"}
              />
              <TextField
                label="Price"
                variant="outlined"
                fullWidth
                margin="normal"
                value={price}
                onChange={handlePriceChange}
                error={errors.price}
                helperText={
                  errors.price && "Price must be a number higher than 0"
                }
              />
              <Box mt={2}>
                <InputLabel htmlFor="image" style={{ marginBottom: 8 }}>
                  Image
                </InputLabel>
                <FormControl fullWidth>
                  <Input
                    type="file"
                    id="image"
                    accept="image/*"
                    onChange={handleImageChange}
                    error={errors.image}
                  />
                  {errors.image && (
                    <Typography color="error">Image is required</Typography>
                  )}
                </FormControl>
              </Box>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 16 }}
              >
                Submit
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddCourse;
