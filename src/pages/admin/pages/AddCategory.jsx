import { useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Box,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_CREATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import CustomButton from "../../../components/Button";
import useLogin from "../../../hooks/useLogin";
import useLanguageData from "../../../hooks/useLanguageData";

const AddCategory = () => {
  const { token } = useLogin();
  const navigate = useNavigate();
  const { setDataLanguage } = useLanguageData();
  const [categoryName, setCategoryName] = useState("");
  const [bannerImage, setBannerImage] = useState(null);
  const [flagImage, setFlagImage] = useState(null);
  const [description, setDescription] = useState("");
  const [errors, setErrors] = useState({
    categoryName: false,
    bannerImage: false,
    flagImage: false,
    description: false,
  });

  const handleCategoryNameChange = (event) => {
    setCategoryName(event.target.value);
  };

  const handleBannerImageChange = (event) => {
    const file = event.target.files[0];
    setBannerImage(file);
  };

  const handleFlagImageChange = (event) => {
    const file = event.target.files[0];
    setFlagImage(file);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Validate required fields
    const newErrors = {
      categoryName: !categoryName,
      bannerImage: !bannerImage,
      flagImage: !flagImage,
      description: !description,
    };

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      await axios.post(
        `${API_URL}categories`,
        {
          CategoryName: categoryName,
          Banner: bannerImage,
          Flag: flagImage,
          Description: description,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );

      Cookies.set(SUCCESS_CREATE, 1, { expires: 1 });
      setCategoryName("");
      setBannerImage(null);
      setFlagImage(null);
      setDescription("");

      const responseActive = await axios.get(`${API_URL}categories/active`);
      setDataLanguage(responseActive.data.categories);

      // const response = await axios.get(`${API_URL}categories`, {
      //   headers: {
      //     Authorization: `Bearer ${token}`,
      //   },
      // });
      // setDataLanguageAdmin(response.data);
      navigate("/admin/category");
    } catch (e) {
      if (e.response.status === 403 || e.response.status === 401) navigate("/");
    }
  };

  return (
    <>
      <Grid container justifyContent="center" gap={2}>
        <Grid item xs={12} md={8} lg={6}>
          <CustomButton
            label="Back"
            float={"left"}
            color="#d80403"
            width="180px"
            onClick={() => navigate("/admin/category")}
          />
        </Grid>
        <Grid item xs={12} md={8} lg={6}>
          <Paper elevation={3} style={{ padding: 16 }}>
            <Typography variant="h5" gutterBottom>
              Add Category
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                label="Category Name"
                variant="outlined"
                fullWidth
                margin="normal"
                value={categoryName}
                onChange={handleCategoryNameChange}
                error={errors.categoryName}
                helperText={errors.categoryName && "Category Name is required"}
              />
              <Box mt={2}>
                <InputLabel htmlFor="banner-image" style={{ marginBottom: 8 }}>
                  Banner
                </InputLabel>
                <FormControl fullWidth>
                  <Input
                    type="file"
                    id="banner-image"
                    accept="image/*"
                    onChange={handleBannerImageChange}
                    error={errors.bannerImage}
                  />
                  {errors.bannerImage && (
                    <Typography color="error">Banner is required</Typography>
                  )}
                </FormControl>
              </Box>
              <Box mt={2}>
                <InputLabel htmlFor="flag-image" style={{ marginBottom: 8 }}>
                  Flag
                </InputLabel>
                <FormControl fullWidth>
                  <Input
                    type="file"
                    id="flag-image"
                    accept="image/*"
                    onChange={handleFlagImageChange}
                    error={errors.flagImage}
                  />
                  {errors.flagImage && (
                    <Typography color="error">Flag is required</Typography>
                  )}
                </FormControl>
              </Box>
              <TextField
                label="Description"
                variant="outlined"
                fullWidth
                multiline
                rows={4}
                margin="normal"
                value={description}
                onChange={handleDescriptionChange}
                error={errors.description}
                helperText={errors.description && "Description is required"}
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 16 }}
              >
                Submit
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddCategory;
