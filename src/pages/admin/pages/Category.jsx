import AdminTable from "../components/AdminTable";
import useAllCategory from "../../../hooks/admin/useAllCategories";
import CustomButton from "../../../components/Button";
import { useNavigate } from "react-router-dom";
import PopupAlert from "../../../components/PopupAlert";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { SUCCESS_CREATE, SUCCESS_UPDATE } from "../../../constants";
import { Typography } from "@mui/material";

const Category = () => {
  const navigate = useNavigate();
  const { dataLanguage } = useAllCategory();
  const tableHead = ["No", "Name", "Image", "Description", "Status", "Action"];
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const handleAddPage = () => {
    navigate("/admin/category/add");
  };

  useEffect(() => {
    if (Cookies.get(SUCCESS_CREATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Create Category");
      Cookies.remove(SUCCESS_CREATE);
    }
    if (Cookies.get(SUCCESS_UPDATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Update Category");
      Cookies.remove(SUCCESS_UPDATE);
    }
  }, []);

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        onClose={() => {
          setShowAlert(false);
        }}
      />
      <CustomButton
        label="Add Category"
        float={"right"}
        color="#66b423"
        width="180px"
        onClick={handleAddPage}
      />
      <Typography mt={6} variant="h5" textAlign={"center"}>
        Categories
      </Typography>
      <AdminTable data={dataLanguage} tableHead={tableHead} type={"category"} />
    </>
  );
};

export default Category;
