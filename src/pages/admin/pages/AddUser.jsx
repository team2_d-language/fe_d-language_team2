import { Button, TextField, Grid, Paper, Typography } from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_CREATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import CustomButton from "../../../components/Button";
import useRegistration from "../../../hooks/useRegistration";
import useLogin from "../../../hooks/useLogin";
import { useState } from "react";
import PopupAlert from "../../../components/PopupAlert";

const AddUser = () => {
  const { token } = useLogin();
  const navigate = useNavigate();
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const {
    name,
    setName,
    nameError,
    email,
    setEmail,
    emailError,
    password,
    setPassword,
    passwordError,
    confirmPassword,
    setConfirmPassword,
    confirmPasswordError,
    handleError,
  } = useRegistration();

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleConfirmPasswordChange = (event) => {
    setConfirmPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (
      name === "" ||
      email === "" ||
      password === "" ||
      confirmPassword === ""
    ) {
      handleError();
      return;
    }

    try {
      await axios.post(
        `${API_URL}create-admin`,
        {
          fullName: name,
          email: email,
          password: password,
          confirmPassword: confirmPassword,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      Cookies.set(SUCCESS_CREATE, 1, { expires: 1 });
      setName("");
      setEmail("");
      setPassword("");
      setConfirmPassword("");
      navigate("/admin/user");
    } catch (e) {
      if (e.response.status === 400) {
        setShowAlert(true);
        setAlertMessage(e.response.data.message);
      }
      if (e.response.status === 403 || e.response.status === 401) navigate("/");
    }
  };

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        severity={"error"}
        onClose={() => {
          setShowAlert(false);
          setAlertMessage("");
        }}
      />
      <Grid container justifyContent="center" gap={2}>
        <Grid item xs={12} md={8} lg={6}>
          <CustomButton
            label="Back"
            float={"left"}
            color="#d80403"
            width="180px"
            onClick={() => navigate("/admin/user")}
          />
        </Grid>
        <Grid item xs={12} md={8} lg={6}>
          <Paper elevation={3} style={{ padding: 16 }}>
            <Typography variant="h5" gutterBottom>
              Add User
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                label="Full Name"
                variant="outlined"
                fullWidth
                margin="normal"
                value={name}
                onChange={handleNameChange}
                error={nameError !== ""}
                helperText={nameError}
              />
              <TextField
                label="Email"
                variant="outlined"
                fullWidth
                margin="normal"
                value={email}
                onChange={handleEmailChange}
                error={emailError !== ""}
                helperText={emailError}
              />
              <TextField
                label="Password"
                variant="outlined"
                fullWidth
                margin="normal"
                type="password"
                value={password}
                onChange={handlePasswordChange}
                error={passwordError !== ""}
                helperText={passwordError}
              />
              <TextField
                label="Confirm Password"
                variant="outlined"
                fullWidth
                margin="normal"
                type="password"
                value={confirmPassword}
                onChange={handleConfirmPasswordChange}
                error={confirmPasswordError !== ""}
                helperText={confirmPasswordError}
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 16 }}
              >
                Submit
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddUser;
