/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Box,
  Select,
  MenuItem,
  Stack,
  Switch,
} from "@mui/material";
import axios from "axios";
import { API_URL, SUCCESS_UPDATE } from "../../../constants";
import Cookies from "js-cookie";
import { useNavigate, useParams } from "react-router-dom";
import CustomButton from "../../../components/Button";
import PopupAlert from "../../../components/PopupAlert";
import nothing from "../../../assets/nothing.svg";
import useLogin from "../../../hooks/useLogin";

const UpdateUser = () => {
  const { token } = useLogin();
  const { id } = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("user");
  const [isActive, setIsActive] = useState(true);
  const [errors, setErrors] = useState({
    name: false,
    email: false,
    role: false,
  });

  const [code, setCode] = useState(200);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get(`${API_URL}users/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const data = response.data.user;
        setName(data.fullName);
        setEmail(data.email);
        setRole(data.role);
        setIsActive(data.isActive);
        setCode(200);
      } catch (error) {
        if (error.response.status === 404) {
          setCode(404);
          setAlertMessage("User Not Found");
          setShowAlert(true);
        }
        if (error.response.status === 403 || error.response.status === 401)
          navigate("/");
      }
    };
    getData();
  }, [id, token]);

  const handleIsActiveChange = () => {
    setIsActive(!isActive);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleRoleChange = (event) => {
    setRole(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const newErrors = {
      name: !name,
      email:
        !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          email
        ),
      role: role !== "admin" && role !== "user",
    };

    setErrors(newErrors);

    if (Object.values(newErrors).some((error) => error)) {
      return;
    }

    try {
      await axios.put(
        `${API_URL}users/${id}`,
        {
          fullName: name,
          email: email,
          role: role,
          isActive: isActive,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      Cookies.set(SUCCESS_UPDATE, 1, { expires: 1 });

      setName("");
      setEmail("");
      setRole("user");
      navigate("/admin/user");
    } catch (e) {
      if (e.response.status === 403 || e.response.status === 401) navigate("/");
    }
  };

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        severity={"error"}
        onClose={() => {
          setShowAlert(false);
          setAlertMessage("");
        }}
      />
      {code === 404 ? (
        <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
          <img src={nothing} alt="nothing" height={200} />
          <Typography textAlign={"center"}>Data not found</Typography>
        </Stack>
      ) : (
        <Grid container justifyContent="center" gap={2}>
          <Grid item xs={12} md={8} lg={6}>
            <CustomButton
              label="Back"
              float={"left"}
              color="#d80403"
              width="180px"
              onClick={() => navigate("/admin/user")}
            />
          </Grid>
          <Grid item xs={12} md={8} lg={6}>
            <Paper elevation={3} style={{ padding: 16 }}>
              <Typography variant="h5" gutterBottom>
                Update User
              </Typography>
              <form onSubmit={handleSubmit}>
                <TextField
                  label=" Full Name"
                  variant="outlined"
                  fullWidth
                  margin="normal"
                  value={name}
                  onChange={handleNameChange}
                  error={errors.name}
                  helperText={errors.name && "Full Name is required"}
                />
                <TextField
                  label="Email"
                  variant="outlined"
                  fullWidth
                  margin="normal"
                  value={email}
                  onChange={handleEmailChange}
                  error={errors.email}
                  helperText={errors.email && "Invalid Email format"}
                />
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Role</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={role}
                    label="role"
                    onChange={handleRoleChange}
                  >
                    <MenuItem value={"user"}>User</MenuItem>
                    <MenuItem value={"admin"}>Admin</MenuItem>
                  </Select>
                </FormControl>
                <Box mt={2}>
                  <Typography variant="body1" gutterBottom>
                    Is Active:
                  </Typography>
                  <Switch
                    checked={isActive}
                    onChange={handleIsActiveChange}
                    inputProps={{ "aria-label": "is active checkbox" }}
                  />
                </Box>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  style={{ marginTop: 16 }}
                >
                  Submit
                </Button>
              </form>
            </Paper>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default UpdateUser;
