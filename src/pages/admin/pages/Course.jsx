import { useNavigate, useParams } from "react-router-dom";
import useAllCoursesByCategory from "../../../hooks/admin/useAllCoursesByCategory.Js";
import AdminTable from "../components/AdminTable";
import PopupAlert from "../../../components/PopupAlert";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { SUCCESS_CREATE, SUCCESS_UPDATE } from "../../../constants";
import CustomButton from "../../../components/Button";
import { Typography } from "@mui/material";

const Course = () => {
  const navigate = useNavigate();
  const { category } = useParams();
  const dataCourse = useAllCoursesByCategory(category);
  const tableHead = [
    "No",
    "Name",
    "Image",
    "Price",
    "Description",
    "Status",
    "Action",
  ];
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const handleAddPage = () => {
    navigate("add");
  };

  useEffect(() => {
    if (Cookies.get(SUCCESS_CREATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Create Course");
      Cookies.remove(SUCCESS_CREATE);
    }
    if (Cookies.get(SUCCESS_UPDATE)) {
      setShowAlert(true);
      setAlertMessage("Successfully Update Course");
      Cookies.remove(SUCCESS_UPDATE);
    }
  }, []);

  return (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        onClose={() => {
          setShowAlert(false);
        }}
      />
      <CustomButton
        label="Add Course"
        float={"right"}
        color="#66b423"
        width="180px"
        onClick={handleAddPage}
      />
      <Typography mt={6} variant="h5" textAlign={"center"}>
        {category} courses
      </Typography>
      <AdminTable
        data={dataCourse}
        tableHead={tableHead}
        type={"course"}
        category={category}
      />
    </>
  );
};

export default Course;
