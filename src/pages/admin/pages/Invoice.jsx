import AdminTable from "../components/AdminTable";
import useAllInvoices from "../../../hooks/admin/useAllInvoices";
import { Typography } from "@mui/material";

const Invoice = () => {
  const dataInvoice = useAllInvoices();
  const tableHead = [
    "No. Invoice",
    "User",
    "Date Transaction",
    "Total Course",
    "Total Price",
    "Payment",
    "Action",
  ];

  return (
    <>
      <Typography mt={6} variant="h5" textAlign={"center"}>
        Invoices
      </Typography>
      <AdminTable data={dataInvoice} tableHead={tableHead} type={"invoice"} />
    </>
  );
};

export default Invoice;
