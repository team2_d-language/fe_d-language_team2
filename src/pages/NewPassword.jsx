/* eslint-disable no-unused-vars */
import CustomButton from "../components/Button";
import {
  Container,
  Grid,
  Typography,
  Stack,
  TextField,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import { useEffect, useState } from "react";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Link, useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import PopupAlert from "../components/PopupAlert";
import Cookies from "js-cookie";
import { API_URL, SUCCESS_RESET_PASSWORD } from "../constants";

const NewPassword = () => {
  const { token } = useParams();
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const [error, setError] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (password.length > 0 && password.length < 8) {
      setPasswordError("Password must be at least 8 characters");
    } else {
      setPasswordError("");
    }
  }, [password]);

  useEffect(() => {
    if (confirmPassword.length > 0 && confirmPassword.length < 8) {
      setConfirmPasswordError("Confirm Password must be at least 8 characters");
    } else {
      setConfirmPasswordError("");
    }
  }, [confirmPassword]);

  useEffect(() => {
    if (confirmPassword !== password) {
      setConfirmPasswordError("Passwords do not match");
    } else {
      setConfirmPasswordError("");
    }
  }, [password, confirmPassword]);

  const handleSubmit = () => {
    if (passwordError || confirmPasswordError) return;
    if (password.length === 0) {
      setPasswordError("Password is required");
      return;
    }
    if (password !== confirmPassword) {
      setConfirmPasswordError("Passwords do not match");
      return;
    }

    const createPassword = async () => {
      try {
        setOpen(true);
        await axios.put(`${API_URL}create-password`, {
          token: token,
          newPassword: password,
          confirmPassword: confirmPassword,
        });
        setOpen(false);
        Cookies.set(SUCCESS_RESET_PASSWORD, 1, { expires: 1 });
        navigate("/login");
      } catch (e) {
        if (e.response.status === 400) {
          setError(e.response.data.message);
        }
        if (e.response.status === 500) {
          setError("Not Allowed");
        }
        setOpen(false);
        setShowAlert(true);
      }
    };
    createPassword();
  };

  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const handleClickShowConfirmPassword = () =>
    setShowConfirmPassword(!showConfirmPassword);
  const handleMouseDownConfirmPassword = () =>
    setShowConfirmPassword(!showConfirmPassword);

  return (
    <>
      <Container maxWidth="md">
        <PopupAlert
          message={error}
          showAlert={showAlert}
          severity="error"
          onClose={() => {
            setShowAlert(false);
            setError("");
          }}
        />
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8} sx={{ p: 0, mt: 6, mb: 3 }}>
            <Typography
              variant="h5"
              style={{
                color: "#323232",
                fontWeight: 0,
                display: "flex",
                alignItems: "center",
                gap: 8,
              }}
              sx={{ mb: 2, pt: 3 }}
            >
              Create Password
            </Typography>
          </Grid>
          <Grid item xs={12} md={12}>
            <Stack spacing={3}>
              <TextField
                fullWidth
                type={showPassword ? "text" : "password"}
                id="newpassword"
                label="New Password"
                variant="outlined"
                size="small"
                sx={{ my: 1 }}
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                error={passwordError != ""}
                helperText={passwordError}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                fullWidth
                type={showConfirmPassword ? "text" : "password"}
                id="confirmnewpassword"
                label="Confirm New Password"
                variant="outlined"
                size="small"
                sx={{ my: 1 }}
                value={confirmPassword}
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                }}
                error={confirmPasswordError != ""}
                helperText={confirmPasswordError}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowConfirmPassword}
                        onMouseDown={handleMouseDownConfirmPassword}
                      >
                        {showConfirmPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />

              <Stack
                spacing={3}
                direction="row"
                justifyContent="flex-end"
                sx={{ mt: "32px", ml: "auto" }}
              >
                <Link to="/forgot-password">
                  <CustomButton
                    label="Cancel"
                    color="#ea9e1f"
                    width="140px"
                    height="38px"
                  />
                </Link>
                {/* {passwordError || confirmPasswordError || password === "" ? (
                  <CustomButton
                    label="Submit"
                    color="#226957"
                    width="140px"
                    height="38px"
                    onClick={handleSubmit}
                  />
                ) : (
                  <Link to={"/login"}>
                  </Link>
                )} */}
                <CustomButton
                  label="Submit"
                  color="#226957"
                  width="140px"
                  height="38px"
                  onClick={handleSubmit}
                />
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default NewPassword;
