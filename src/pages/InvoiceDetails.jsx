/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import {
  CssBaseline,
  Typography,
  Box,
  Stack,
  Container,
  useMediaQuery,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import { Link, useNavigate, useParams } from "react-router-dom";
import TableComponent from "../components/TableComponent";
import Cookies from "js-cookie";
import { UNAUTHORIZED } from "../constants";
import useInvoiceDetails from "../hooks/useInvoiceDetails";
import { formatDate } from "../helper/dateConvertion";
import { currencyConvertion } from "../helper/currencyConvertion";
import { useEffect } from "react";
import CustomButton from "../components/Button";
import useLogin from "../hooks/useLogin";

const InvoiceDetails = ({ admin = false }) => {
  const { isLogin } = useLogin();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLogin) {
      Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
      navigate("/");
    }
  }, [isLogin]);

  const { id } = useParams();
  const { dataInvoiceDetails, error } = useInvoiceDetails(id);
  const isSuperSmallScreen = useMediaQuery("(max-width:400px)");
  const isSmallScreen = useMediaQuery("(max-width:660px)");
  const tableHead = ["No", "Course Name", "Language", "Schedule", "Price"];

  return (
    <Container maxWidth="xl" sx={{ my: admin ? 5 : 14 }}>
      <CssBaseline />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={!isLogin}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {isLogin && (
        <>
          {!admin && (
            <Stack px={4} mb={3} direction={"row"} gap={1}>
              <Link
                to="/"
                style={{
                  textDecoration: "none",
                  color: "#828282",
                  cursor: "pointer",
                }}
              >
                <Typography style={{ fontWeight: "bold" }}>Home</Typography>
              </Link>
              <Typography style={{ fontWeight: "bold" }} color={"#828282"}>
                &gt;
              </Typography>
              <Link
                to="/invoice"
                style={{
                  textDecoration: "none",
                  color: "#828282",
                  cursor: "pointer",
                }}
              >
                <Typography style={{ fontWeight: "bold" }}>Invoice</Typography>
              </Link>
              <Typography style={{ fontWeight: "bold" }} color={"#828282"}>
                &gt;
              </Typography>
              <Typography
                style={{
                  fontWeight: "bold",
                  cursor: "pointer",
                  color: "#ea9e1f",
                }}
              >
                Details Invoice
              </Typography>
            </Stack>
          )}

          {error === 400 ? (
            <Typography
              px={4}
              mt={5}
              align={"center"}
              fontWeight={"bold"}
              variant={"h5"}
            >
              Details Invoice Not Found
            </Typography>
          ) : (
            <>
              {admin ? (
                <Stack mb={5} fontWeight={"bold"} variant={"h6"}>
                  <CustomButton
                    label="Back"
                    float={"left"}
                    color="#d80403"
                    width="150px"
                    onClick={() => navigate("/admin/invoice")}
                  />
                  <Typography
                    mt={5}
                    fontWeight={"bold"}
                    variant={"h5"}
                    textAlign={"center"}
                  >
                    Details Invoice
                  </Typography>
                </Stack>
              ) : (
                <Typography
                  px={admin ? 0 : 4}
                  mb={5}
                  fontWeight={"bold"}
                  variant={"h6"}
                >
                  Details Invoice
                </Typography>
              )}

              <Box px={admin ? 0 : 4}>
                <Stack direction={"row"} gap={3} mb={1}>
                  <div style={{ width: isSuperSmallScreen ? "80px" : "100px" }}>
                    <Typography
                      variant={isSuperSmallScreen ? "body2" : "body1"}
                    >
                      No. Invoice:
                    </Typography>
                  </div>
                  <Typography variant={isSuperSmallScreen ? "body2" : "body1"}>
                    {dataInvoiceDetails.invoiceNumber}
                  </Typography>
                </Stack>
                <Stack
                  direction={isSmallScreen ? "column" : "row"}
                  justifyContent={"space-between"}
                  mb={5}
                >
                  <Stack direction={"row"} gap={3} mb={1}>
                    <div
                      style={{ width: isSuperSmallScreen ? "80px" : "100px" }}
                    >
                      <Typography
                        variant={isSuperSmallScreen ? "body2" : "body1"}
                      >
                        Date:
                      </Typography>
                    </div>
                    <Typography
                      variant={isSuperSmallScreen ? "body2" : "body1"}
                    >
                      {formatDate(dataInvoiceDetails.date, false)}
                    </Typography>
                  </Stack>
                  <Stack direction={"row"} gap={3} mb={1}>
                    <div
                      style={{ width: isSuperSmallScreen ? "80px" : "100px" }}
                    >
                      <Typography
                        fontWeight={"bold"}
                        variant={isSuperSmallScreen ? "body2" : "body1"}
                      >
                        Total Price
                      </Typography>
                    </div>
                    <Typography
                      fontWeight={"bold"}
                      variant={isSuperSmallScreen ? "body2" : "body1"}
                    >
                      IDR{" "}
                      {dataInvoiceDetails.totalPrice
                        ? currencyConvertion(dataInvoiceDetails.totalPrice)
                        : ""}
                    </Typography>
                  </Stack>
                </Stack>
                <TableComponent
                  dataTable={dataInvoiceDetails.detail}
                  tableHead={tableHead}
                  detailInvoice={true}
                />
              </Box>
            </>
          )}
        </>
      )}
    </Container>
  );
};

export default InvoiceDetails;
