/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { useContext, useEffect, useState } from "react";
import {
  Checkbox,
  Container,
  Divider,
  Typography,
  Stack,
  useMediaQuery,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import nothing from "../assets/nothing.svg";
import CheckoutFooter from "../components/CheckoutFooter";
import PaymentModal from "../components/PaymentModal";
import CheckoutList from "../components/CheckoutList";
import axios from "axios";
import { API_URL, UNAUTHORIZED } from "../constants";
import Cookies from "js-cookie";
import PopupAlert from "../components/PopupAlert";
import { useNavigate } from "react-router-dom";
import usePaymentData from "../hooks/usePaymentData";
import useLogin from "../hooks/useLogin";

export default function Checkout() {
  const navigate = useNavigate();
  const { token } = useLogin();
  const dataPayment = usePaymentData();
  const [data, setData] = useState([]);
  const [selectedPayment, setSelectedPayment] = useState("0");
  const [checked, setChecked] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [checkedAll, setCheckedAll] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState(true);
  const isSmallScreen = useMediaQuery("(max-width:550px)");
  const [open, setOpen] = useState(false);
  const [error, setError] = useState(401);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    const getData = async () => {
      try {
        const responseCart = await axios.get(`${API_URL}carts`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setData(responseCart.data);
        setError(200);
      } catch (error) {
        if (error.response.status === 401) {
          setError(error.response.status);
          Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
          navigate("/");
        }
        if (error.response.status === 404) {
          setError(200);
        }
      }
    };
    getData();
  }, [token]);

  const countTotal = (newChecked) => {
    let total = 0;
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < newChecked.length; j++)
        if (data[i].id === newChecked[j]) total += data[i].coursePrice;
    }
    return total;
  };

  const handlePayNow = async () => {
    try {
      if (checked.length === 0) {
        setErrorMessage(true);
        setAlertMessage("Must select cart at least 1");
        setShowAlert(true);
        return;
      }
      await axios.post(
        `${API_URL}invoice`,
        {
          paymentId: selectedPayment,
          cartIds: checked,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      navigate("/success");
    } catch (e) {
      setErrorMessage(true);
      setAlertMessage(e.response.data);
      setShowAlert(true);
    }
  };

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
    setCheckedAll(newChecked.length === data.length);
    setTotalPrice(countTotal(newChecked));
  };

  const handleDelete = (value) => () => {
    const deleteFromCart = async () => {
      try {
        const response = await axios.delete(`${API_URL}carts/${value}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setAlertMessage(response.data.message);
        setErrorMessage(false);
        setShowAlert(true);
      } catch (error) {
        console.log(error);
      }
    };
    deleteFromCart();
    const updatedDataCourse = data.filter((item) => item.id !== value);
    const updatedCheck = checked.filter((item) => item !== value);
    setData(updatedDataCourse);
    setChecked(updatedCheck);
    setCheckedAll(updatedCheck.length === data.length);
    setTotalPrice(countTotal(updatedCheck));
  };

  const handleSelectAll = () => {
    if (!checkedAll) {
      const newChecked = [];
      for (const dat of data) newChecked.push(dat.id);
      setChecked(newChecked);
      setTotalPrice(countTotal(newChecked));
    } else {
      setChecked([]);
      setTotalPrice(0);
    }
    setCheckedAll(!checkedAll);
  };

  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={error === 401}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {(error === 200 || error === 404) && (
        <>
          <Container maxWidth="xl" sx={{ my: 14 }}>
            <PopupAlert
              message={alertMessage}
              showAlert={showAlert}
              severity={errorMessage ? "error" : "success"}
              onClose={() => {
                setShowAlert(false);
                setAlertMessage("");
                setErrorMessage(false);
              }}
            />
            <Stack direction="row" alignItems="center" spacing={4}>
              <Checkbox
                checked={checkedAll}
                disableRipple
                onClick={handleSelectAll}
                disabled={data.length === 0}
              />
              <Typography>Pilih Semua</Typography>
            </Stack>
            <Divider />

            {/* Checkout List */}
            {data.length !== 0 ? (
              <CheckoutList
                data={data}
                handleDelete={handleDelete}
                handleToggle={handleToggle}
                checked={checked}
                isSmallScreen={isSmallScreen}
              />
            ) : (
              <Stack
                sx={{
                  my: 6,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <img src={nothing} alt="nothing" height={200} />
                <Typography
                  variant="h6"
                  mt={4}
                  color={"#b1b1b1"}
                  textAlign="center"
                >
                  Your Cart Is Empty
                </Typography>
              </Stack>
            )}
          </Container>

          {/* Footer */}
          <CheckoutFooter
            totalPrice={totalPrice}
            handleOpen={handleOpen}
            isSmallScreen={isSmallScreen}
            disableBtn={checked.length === 0 ? true : false}
          />

          {/* Modal */}
          <PaymentModal
            open={open}
            handleClose={handleClose}
            handlePayNow={handlePayNow}
            dataPayment={dataPayment}
            selectedPayment={selectedPayment}
            setSelectedPayment={setSelectedPayment}
          />
        </>
      )}
    </>
  );
}
