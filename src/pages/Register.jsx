import {
  Backdrop,
  CircularProgress,
  Container,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import CustomButton from "../components/Button";
import PopupAlert from "../components/PopupAlert";
import useRegistration from "../hooks/useRegistration";

const Register = () => {
  const {
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    passwordError,
    nameError,
    confirmPasswordError,
    emailError,
    registerError,
    setRegisterError,
    showAlert,
    showSuccessAlert,
    open,
    handleError,
    handleSignUp,
    setShowAlert,
    setShowSuccessAlert,
  } = useRegistration();

  return (
    <>
      <Container maxWidth="md">
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <PopupAlert
          message={registerError}
          showAlert={showAlert}
          severity="error"
          onClose={() => {
            setRegisterError("");
            setShowAlert(false);
          }}
        />
        <PopupAlert
          message={
            "Congratulations, your account has been successfull created. Please check your email for confirmation."
          }
          showAlert={showSuccessAlert}
          severity="success"
          onClose={() => {
            setShowSuccessAlert(false);
          }}
        />
        <Grid container spacing={2}>
          <Grid item xs={12} md={8} sx={{ p: 0, mt: 6 }}>
            <Typography
              variant="h5"
              style={{
                color: "#226957",
                fontWeight: 400,
                display: "flex",
                alignItems: "center",
                gap: 8,
              }}
              sx={{ mb: 2 }}
            >
              Lets Join{" "}
              <span style={{ color: "#ea9e1f", fontSize: "30px" }}>
                D&apos;Language
              </span>
            </Typography>
            <Typography color="#757575" mb={2}>
              Please register first.
            </Typography>
          </Grid>
          <Grid item xs={12} md={12}>
            <Stack spacing={3}>
              <TextField
                fullWidth
                id="name"
                label="Name"
                variant="outlined"
                size="small"
                value={name}
                onChange={(e) => setName(e.target.value)}
                error={nameError !== ""}
                helperText={nameError}
                sx={{ mb: 1 }}
              />
              <TextField
                fullWidth
                id="email"
                label="Email"
                variant="outlined"
                size="small"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                error={emailError !== ""}
                helperText={emailError}
                sx={{ my: 1 }}
              />
              <TextField
                fullWidth
                id="password"
                label="Password"
                variant="outlined"
                size="small"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                error={passwordError !== ""}
                helperText={passwordError}
                sx={{ my: 1 }}
              />
              <TextField
                fullWidth
                id="cpassword"
                label="Confirm Password"
                variant="outlined"
                size="small"
                type="password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                error={confirmPasswordError !== ""}
                helperText={confirmPasswordError}
                sx={{ my: 1 }}
              />
              <div
                style={{
                  marginTop: "32px",
                  marginBottom: "40px",
                  textAlign: "end",
                }}
              >
                {nameError ||
                emailError ||
                passwordError ||
                confirmPasswordError ||
                name === "" ||
                email === "" ||
                password === "" ||
                confirmPassword === "" ? (
                  <CustomButton
                    label="Sign Up"
                    color="#226957"
                    width="140px"
                    height="38px"
                    onClick={handleError}
                  />
                ) : (
                  <CustomButton
                    label="Sign Up"
                    color="#226957"
                    width="140px"
                    height="38px"
                    onClick={handleSignUp}
                  />
                )}
              </div>
              <Typography sx={{ textAlign: "center" }}>
                Have an account?{" "}
                <Link
                  to="/login"
                  style={{ textDecoration: "none", color: "#2f80ed" }}
                >
                  Login here
                </Link>
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Register;
