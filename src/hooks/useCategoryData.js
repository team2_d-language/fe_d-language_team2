/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import { API_URL, UNAUTHORIZED } from "../constants";
import useLogin from "./useLogin";

const useCategoryData = (id) => {
  const { token } = useLogin();
  const navigate = useNavigate();
  const [dataLanguage, setDataLanguage] = useState({});
  const [dataCourse, setDataCourse] = useState([]);
  const [error, setError] = useState(false);
  const [unauthorized, setUnauthorized] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const responseLanguage = await axios.get(
          `${API_URL}category-active/${id}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        setDataLanguage(responseLanguage.data);
        setUnauthorized(false);
      } catch (error) {
        if (error.response.status === 404) {
          setError(true);
          setUnauthorized(false);
        }
        if (error.response.status === 401) {
          setUnauthorized(true);
          Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
          navigate("/");
        }
      }
    };
    fetchData();
  }, [id, token]);

  useEffect(() => {
    const getDataByCategory = async () => {
      try {
        const response = await axios.get(
          `${API_URL}courses/category/${dataLanguage.categoryName}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        setDataCourse(response.data.courses);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    getDataByCategory();
  }, [dataLanguage, token]);

  return { unauthorized, dataLanguage, dataCourse, error };
};

export default useCategoryData;
