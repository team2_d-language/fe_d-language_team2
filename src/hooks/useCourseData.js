import { useContext } from "react";
import { CourseContext } from "../contexts/CourseContext";

const useCourseData = () => {
  const { dataCourse, setDataCourse } = useContext(CourseContext);

  return { dataCourse, setDataCourse };
};

export default useCourseData;
