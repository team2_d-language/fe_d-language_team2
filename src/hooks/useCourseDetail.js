/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { useContext, useEffect, useState } from "react";
import axios from "axios";
import { API_URL, UNAUTHORIZED } from "../constants";
import useLogin from "./useLogin";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";

const useCourseDetail = (id) => {
  const { token } = useLogin();
  const navigate = useNavigate();
  const [detailCourse, setDetailCourse] = useState([]);
  const [relatedCourse, setRelatedCourse] = useState([]);
  const [category, setCategory] = useState();
  const [error, setError] = useState(false);
  const [unauthorized, setUnauthorized] = useState(true);

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get(`${API_URL}course-active/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setCategory(response.data.course.categoryName);
        setDetailCourse(response.data.course);
        setUnauthorized(false);
      } catch (error) {
        if (error.response.status === 404) {
          setUnauthorized(false);
          setError(true);
        }
        if (error.response.status === 401) {
          setUnauthorized(true);
          Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
          navigate("/");
        }
      }
    };
    getData();
  }, [id, token]);

  useEffect(() => {
    const getDataByCategory = async () => {
      try {
        const response = await axios.get(
          `${API_URL}courses/category/${category}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = response.data.courses;
        const filteredData = data.filter((item) => item.id !== id);
        setRelatedCourse(filteredData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    getDataByCategory();
  }, [category, id, token]);

  return { unauthorized, detailCourse, relatedCourse, error };
};

export default useCourseDetail;
