/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL } from "../constants";

const useRegistration = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [nameError, setNameError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [registerError, setRegisterError] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (name.length > 0 && name.length < 3) {
      setNameError("Name must be at least 3 characters");
    } else {
      setNameError("");
    }
  }, [name]);

  useEffect(() => {
    if (password.length > 0 && password.length < 8) {
      setPasswordError("Password must be at least 8 characters");
    } else {
      setPasswordError("");
    }
  }, [password]);

  useEffect(() => {
    if (confirmPassword.length > 0 && confirmPassword !== password) {
      setConfirmPasswordError("Passwords do not match");
    } else {
      setConfirmPasswordError("");
    }
  }, [confirmPassword, password]);

  useEffect(() => {
    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.length > 0 && !emailRegex.test(email)) {
      setEmailError("Invalid email format");
    } else {
      setEmailError("");
    }
  }, [email]);

  const handleError = () => {
    if (name.length === 0) {
      setNameError("Name cannot be empty");
    }

    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.length === 0 || !emailRegex.test(email)) {
      setEmailError("Invalid email format");
    }

    if (password.length === 0 || password.length < 8) {
      setPasswordError("Password must be at least 8 characters");
    }

    if (confirmPassword.length === 0 || confirmPassword !== password) {
      setConfirmPasswordError("Passwords do not match");
    }
  };

  const handleSignUp = async () => {
    if (!nameError && !emailError && !passwordError && !confirmPasswordError) {
      try {
        setOpen(true);
        await axios.post(`${API_URL}register`, {
          fullName: name,
          email: email,
          password: password,
          confirmPassword: confirmPassword,
        });
        setOpen(false);
        setShowSuccessAlert(true);
        setName("");
        setEmail("");
        setPassword("");
        setConfirmPassword("");
      } catch (error) {
        setOpen(false);
        setRegisterError(error.response.data.message);
        setShowAlert(true);
      }
    }
  };

  return {
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    passwordError,
    nameError,
    confirmPasswordError,
    emailError,
    registerError,
    setRegisterError,
    showAlert,
    showSuccessAlert,
    open,
    handleError,
    handleSignUp,
    setShowAlert,
    setShowSuccessAlert,
  };
};

export default useRegistration;
