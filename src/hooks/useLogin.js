import { useContext } from "react";
import { AuthContext } from "../contexts/authContext";

const useLogin = () => {
  const {
    token,
    role,
    isLogin,
    loading,
    login,
    handleLogout,
    navigate,
    email,
    setEmail,
    password,
    setPassword,
    loginError,
    setLoginError,
    showAlertLoginFail,
    setShowAlertLoginFail,
  } = useContext(AuthContext);

  return {
    token,
    role,
    isLogin,
    loading,
    login,
    handleLogout,
    navigate,
    email,
    setEmail,
    password,
    setPassword,
    loginError,
    setLoginError,
    showAlertLoginFail,
    setShowAlertLoginFail,
  };
};

export default useLogin;
