import { useContext } from "react";
import { CategoryContext } from "../contexts/CategoryContext";

const useLanguageData = () => {
  const { dataLanguage, setDataLanguage } = useContext(CategoryContext);

  return { dataLanguage, setDataLanguage };
};

export default useLanguageData;
