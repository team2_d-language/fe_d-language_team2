/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import useLogin from "../useLogin";

const useAllUsers = () => {
  const { token, navigate } = useLogin();
  const [dataUser, setDataUser] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${API_URL}users`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataUser(response.data.users);
      } catch (error) {
        if (error.response.status == 403 || error.response.status == 401)
          navigate("/");
      }
    };
    fetchData();
  }, [token]);
  return dataUser;
};

export default useAllUsers;
