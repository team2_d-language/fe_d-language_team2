/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import useLogin from "../useLogin";

const useAllCategories = () => {
  const { token, navigate } = useLogin();
  const [dataLanguage, setDataLanguage] = useState([]);
  const [forbidden, setForbidden] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${API_URL}categories`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setForbidden(false);
        setDataLanguage(response.data);
      } catch (error) {
        if (error.response.status == 403 || error.response.status == 401) {
          setForbidden(true);
          navigate("/");
        }
      }
    };
    fetchData();
  }, [token]);
  return { dataLanguage, setDataLanguage, forbidden };
};

export default useAllCategories;
