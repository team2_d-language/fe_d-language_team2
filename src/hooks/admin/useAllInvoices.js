/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import useLogin from "../useLogin";

const useAllInvoices = () => {
  const { token, navigate } = useLogin();
  const [dataInvoice, setDataInvoice] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${API_URL}invoice-all`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataInvoice(response.data);
      } catch (error) {
        if (error.response.status == 403 || error.response.status == 401)
          navigate("/");
      }
    };
    fetchData();
  }, [token]);
  return dataInvoice;
};

export default useAllInvoices;
