/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import useLogin from "../useLogin";

const useAllPayments = () => {
  const { token, navigate } = useLogin();
  const [dataPayment, setDataPayment] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${API_URL}payments`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataPayment(response.data);
      } catch (error) {
        if (error.response.status == 403 || error.response.status == 401)
          navigate("/");
      }
    };
    fetchData();
  }, [token]);
  return dataPayment;
};

export default useAllPayments;
