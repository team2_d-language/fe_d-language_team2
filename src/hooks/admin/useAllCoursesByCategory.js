/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import useLogin from "../useLogin";

const useAllCoursesByCategory = (category) => {
  const { token, navigate } = useLogin();
  const [dataCourse, setDataCourse] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${API_URL}courses/category-all/${category}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        setDataCourse(response.data.courses);
      } catch (error) {
        if (error.response.status == 403 || error.response.status == 401)
          navigate("/");
      }
    };
    fetchData();
  }, [token, category]);
  return dataCourse;
};

export default useAllCoursesByCategory;
