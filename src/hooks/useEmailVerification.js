import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL } from "../constants";

const useEmailVerification = (token) => {
  const [isValid, setIsValid] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    const verifyEmail = async () => {
      try {
        const response = await axios.post(`${API_URL}verify`, {
          token: token,
        });

        if (response.status === 200) {
          setIsValid(true);
          setError("");
        }
      } catch (e) {
        setIsValid(false);
        setError("Something went wrong");
      }
    };

    verifyEmail();
  }, [token]);

  return { isValid, error };
};

export default useEmailVerification;
