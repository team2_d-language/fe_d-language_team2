import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL } from "../constants";
import useLogin from "./useLogin";

const usePaymentData = () => {
  const { token } = useLogin();
  const [dataPayment, setDataPayment] = useState([]);

  useEffect(() => {
    const fetchPaymentData = async () => {
      try {
        const response = await axios.get(`${API_URL}payments/active`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataPayment(response.data);
      } catch (error) {
        console.error("Error fetching payment data:", error);
      }
    };

    fetchPaymentData();
  }, [token]);

  return dataPayment;
};

export default usePaymentData;
