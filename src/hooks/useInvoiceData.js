/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL, UNAUTHORIZED } from "../constants";
import useLogin from "./useLogin";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

const useInvoiceData = () => {
  const { token } = useLogin();
  const [dataInvoice, setDataInvoice] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchInvoiceData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(`${API_URL}invoice`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataInvoice(response.data);
        setError(200);
      } catch (error) {
        setError(error.response.status);
        Cookies.set(UNAUTHORIZED, 1, { expires: 1 });
        navigate("/");
      } finally {
        setIsLoading(false);
      }
    };

    fetchInvoiceData();
  }, [token]);

  return { dataInvoice, isLoading, error };
};

export default useInvoiceData;
