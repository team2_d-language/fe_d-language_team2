import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL } from "../constants";
import useLogin from "./useLogin";

const useInvoiceDetails = (id) => {
  const { token } = useLogin();
  const [dataInvoiceDetails, setDataInvoiceDetails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchInvoiceData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(`${API_URL}invoice/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setDataInvoiceDetails(response.data);
      } catch (error) {
        console.error("Error fetching invoice data:", error);
        setError(error.response.status);
      } finally {
        setIsLoading(false);
      }
    };

    fetchInvoiceData();
  }, [token, id]);

  return { dataInvoiceDetails, isLoading, error };
};

export default useInvoiceDetails;
