import "./App.css";
import Login from "./pages/Login";
import Register from "./pages/Register";
import NewPassword from "./pages/NewPassword";
import ForgotPassword from "./pages/ForgotPassword";
import ListMenuClass from "./pages/ListMenuClass";
import { Routes, Route } from "react-router-dom";
import Landing from "./pages/Landing";
import Checkout from "./pages/Checkout";
import Success from "./pages/Success";
import { NavWithFoot, NavWithoutFoot } from "./templates/Template";
import CourseDetail from "./pages/CourseDetail";
import ConfirmedSucces from "./pages/ConfirmedSucces";
import Invoice from "./pages/Invoice";
import MyClass from "./pages/MyClass";
import InvoiceDetails from "./pages/InvoiceDetails";
import AdminDashboard from "./pages/admin/templates/AdminDashboard";
import Category from "./pages/admin/pages/Category";
import Course from "./pages/admin/pages/Course";
import User from "./pages/admin/pages/User";
import AddCategory from "./pages/admin/pages/AddCategory";
import UpdateCategory from "./pages/admin/pages/UpdateCategory";
import AddCourse from "./pages/admin/pages/AddCourse";
import UpdateCourse from "./pages/admin/pages/UpdateCourse";
import UpdateUser from "./pages/admin/pages/UpdateUser";
import NotFound from "./pages/NotFound";
import Payment from "./pages/admin/pages/Payment";
import AddPayment from "./pages/admin/pages/AddPayment";
import UpdatePayment from "./pages/admin/pages/UpdatePayment";
import AddUser from "./pages/admin/pages/AddUser";
import InvoiceAdmin from "./pages/admin/pages/Invoice";

function App() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <NavWithFoot>
            <Landing />
          </NavWithFoot>
        }
      />
      <Route
        path="/login"
        element={
          <NavWithoutFoot>
            <Login />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/register"
        element={
          <NavWithoutFoot>
            <Register />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/new-password/:token"
        element={
          <NavWithoutFoot>
            <NewPassword />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/forgot-password"
        element={
          <NavWithoutFoot>
            <ForgotPassword />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/checkout"
        element={
          <NavWithoutFoot>
            <Checkout />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/success"
        element={
          <NavWithoutFoot>
            <Success />
          </NavWithoutFoot>
        }
      />
      <Route
        path="/list-menu-class/:id"
        element={
          <NavWithFoot>
            <ListMenuClass />
          </NavWithFoot>
        }
      />
      <Route
        path="/detail-course/:id"
        element={
          <NavWithFoot>
            <CourseDetail />
          </NavWithFoot>
        }
      />
      <Route
        path="/invoice"
        element={
          <NavWithFoot>
            <Invoice />
          </NavWithFoot>
        }
      />
      <Route
        path="/details/:id"
        element={
          <NavWithFoot>
            <InvoiceDetails />
          </NavWithFoot>
        }
      />
      <Route
        path="/my-class"
        element={
          <NavWithFoot>
            <MyClass />
          </NavWithFoot>
        }
      />
      <Route path="/confirm/:token" element={<ConfirmedSucces />} />

      <Route path="/admin/*" element={<AdminDashboard />}>
        <Route index element={<Category />} />
        <Route path="category" element={<Category />} />
        <Route path="category/add" element={<AddCategory />} />
        <Route path="category/update/:id" element={<UpdateCategory />} />
        <Route path="course/:category" element={<Course />} />
        <Route path="course/:category/add" element={<AddCourse />} />
        <Route path="course/:category/:id/update/" element={<UpdateCourse />} />
        <Route path="user" element={<User />} />
        <Route path="user/add" element={<AddUser />} />
        <Route path="user/update/:id" element={<UpdateUser />} />
        <Route path="payment" element={<Payment />} />
        <Route path="payment/add" element={<AddPayment />} />
        <Route path="payment/update/:id" element={<UpdatePayment />} />
        <Route path="invoice" element={<InvoiceAdmin />} />
        <Route path="invoice/:id" element={<InvoiceDetails admin={true} />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
