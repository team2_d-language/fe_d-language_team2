/* eslint-disable react/prop-types */
import {
  Typography,
  Grid,
  Card,
  CardContent,
  CardMedia,
  Stack,
  CircularProgress,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import "./style.css";
import { API_URL } from "../constants";
import PropTypes from "prop-types";
import useLogin from "../hooks/useLogin";

const LanguageList = ({ dataLanguage, setShowAlertUnauthorized }) => {
  const { isLogin } = useLogin();
  const navigate = useNavigate();

  const handleGoTolist = (id) => {
    if (!isLogin) {
      setShowAlertUnauthorized(true);
      return;
    }
    navigate(`/list-menu-class/${id}`);
  };

  return dataLanguage.length === 0 ? (
    <Stack sx={{ alignItems: "center", my: 5 }}>
      <CircularProgress />
    </Stack>
  ) : (
    <Grid container spacing={4} sx={{ px: { xs: 4, md: 16 }, py: 6, mb: 10 }}>
      {dataLanguage.map((data) => (
        <Grid key={data.id} item xs={12} sm={6} md={4} lg={3}>
          <Card
            className="cardHovered"
            sx={{
              cursor: "pointer",
              border: "1px solid #BDBDBD",
              borderRadius: 2,
              boxShadow: "none",
            }}
          >
            <CardActionArea onClick={() => handleGoTolist(data.id)}>
              <CardMedia
                sx={{
                  height: 135,
                  m: 2,
                  borderRadius: 2,
                  border: "1px solid #BDBDBD",
                }}
                image={`${API_URL}${data.flag}`}
                title="img"
              />
              <CardContent>
                <Typography
                  variant="subtitle1"
                  component="div"
                  textAlign="center"
                  sx={{ my: -1, fontSize: 20 }}
                >
                  {data.categoryName}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

LanguageList.propTypes = {
  dataLanguage: PropTypes.arrayOf(PropTypes.object),
  setShowAlertUnauthorized: PropTypes.func,
};

export default LanguageList;
