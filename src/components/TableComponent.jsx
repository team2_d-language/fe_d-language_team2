/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material";
import PropTypes from "prop-types";
import "./style.css";
import CustomButton from "./Button";
import { formatDate } from "../helper/dateConvertion";
import { Link } from "react-router-dom";
import { currencyConvertion } from "../helper/currencyConvertion";

const TableComponent = ({ dataTable, tableHead, detailInvoice = false }) => {
  return (
    <TableContainer
      component={Paper}
      sx={{ border: "none", borderRadius: 0, boxShadow: "none" }}
    >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead sx={{ background: "#226957" }}>
          <TableRow>
            {tableHead.map((headLabel, i) => (
              <TableCell key={i} className="tableText" sx={{ color: "#fff" }}>
                {headLabel}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {!dataTable || dataTable.length === 0 ? (
            <TableRow>
              <TableCell colSpan={6} align="center">
                No data available
              </TableCell>
            </TableRow>
          ) : !detailInvoice ? (
            dataTable.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.invoiceNumber}</TableCell>
                <TableCell className="tableText">
                  {formatDate(row.date, false)}
                </TableCell>
                <TableCell className="tableText">{row.totalCourse}</TableCell>
                <TableCell className="tableText">
                  IDR {currencyConvertion(row.totalPrice)}
                </TableCell>
                <TableCell className="tableText">
                  <Link to={`/details/${row.id}`}>
                    <CustomButton color="#ea9e1f" width="80%" label="Details" />
                  </Link>
                </TableCell>
              </TableRow>
            ))
          ) : (
            dataTable.map((row, i) => (
              <TableRow
                key={i}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="tableRow"
              >
                <TableCell className="tableText" component="th" scope="row">
                  {i + 1}
                </TableCell>
                <TableCell className="tableText">{row.courseName}</TableCell>
                <TableCell className="tableText">{row.language}</TableCell>
                <TableCell className="tableText">
                  {formatDate(row.schedule)}
                </TableCell>
                <TableCell className="tableText">
                  IDR {currencyConvertion(row.price)}
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

TableComponent.propTypes = {
  dataTable: PropTypes.arrayOf(PropTypes.object),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  detailInvoice: PropTypes.bool,
};

export default TableComponent;
