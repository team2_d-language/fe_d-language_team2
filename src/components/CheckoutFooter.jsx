/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Paper, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import CustomButton from "./Button";
import PropTypes from "prop-types";
import { currencyConvertion } from "../helper/currencyConvertion";

const CheckoutFooter = ({
  totalPrice,
  handleOpen,
  isSmallScreen,
  disableBtn = false,
}) => {
  return (
    <Paper
      sx={{ position: "fixed", bottom: 0, left: 0, right: 0, px: 6, py: 3 }}
      elevation={7}
    >
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <Stack
          direction={isSmallScreen ? "column" : "row"}
          spacing={isSmallScreen ? 1 : 3}
        >
          <Typography>Pilih Semua</Typography>
          <Typography color="#226957" fontWeight="bold" variant="body1">
            IDR {currencyConvertion(totalPrice)}
          </Typography>
        </Stack>
        <CustomButton
          color={disableBtn ? "#ddd" : "#226957"}
          label="Pay Now"
          width="200px"
          onClick={handleOpen}
          disable={disableBtn}
        />
      </Stack>
    </Paper>
  );
};

CheckoutFooter.propTypes = {
  totalPrice: PropTypes.number.isRequired,
  handleOpen: PropTypes.func.isRequired,
  isSmallScreen: PropTypes.bool.isRequired,
  disableBtn: PropTypes.bool,
};

export default CheckoutFooter;
