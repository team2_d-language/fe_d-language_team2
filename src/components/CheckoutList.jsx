/* eslint-disable react/prop-types */
import {
  Typography,
  List,
  ListItem,
  ListItemText,
  Stack,
  IconButton,
  ListItemButton,
  ListItemIcon,
  Checkbox,
  Divider,
} from "@mui/material";
import trashLogo from "../assets/trashLogo.svg";
import { API_URL } from "../constants";
import PropTypes from "prop-types";
import { formatDate } from "../helper/dateConvertion";
import { currencyConvertion } from "../helper/currencyConvertion";

const CheckoutList = ({
  data,
  handleDelete,
  handleToggle,
  checked,
  isSmallScreen,
}) => {
  return (
    <List>
      {data.map((data) => {
        const labelId = `checkbox-list-label-${data.id}`;

        return (
          <div key={data.id}>
            <ListItem
              secondaryAction={
                <IconButton
                  edge="end"
                  aria-label="comments"
                  onClick={handleDelete(data.id)}
                >
                  <img src={trashLogo} alt={"trash"} />
                </IconButton>
              }
              disablePadding
              sx={{ my: 2 }}
            >
              <ListItemButton
                role={undefined}
                onClick={handleToggle(data.id)}
                dense
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={checked.indexOf(data.id) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                <Stack direction={isSmallScreen ? "column" : "row"}>
                  <img
                    width={200}
                    src={`${API_URL}${data.courseImage}`}
                    alt={data.courseName}
                  />
                  <Stack ml={isSmallScreen ? 0 : 3}>
                    <ListItemText
                      id={labelId}
                      primary={
                        <Typography color="#828282" variant="subtitle1" mb={-1}>
                          {data.categoryName}
                        </Typography>
                      }
                    />
                    <ListItemText
                      id={labelId}
                      primary={
                        <Typography variant="h6" sx={{ fontWeight: "bold" }}>
                          {data.courseName}
                        </Typography>
                      }
                    />
                    <ListItemText
                      id={labelId}
                      primary={
                        <Typography variant="body2" color="#4f4f4f">
                          Schedule : {formatDate(data.schedule)}
                        </Typography>
                      }
                    />
                    <ListItemText
                      id={labelId}
                      primary={
                        <Typography
                          color="#ea9e1f"
                          fontWeight="bold"
                          variant="body1"
                        >
                          IDR {currencyConvertion(data.coursePrice)}
                        </Typography>
                      }
                    />
                  </Stack>
                </Stack>
              </ListItemButton>
            </ListItem>
            <Divider />
          </div>
        );
      })}
    </List>
  );
};

CheckoutList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  handleDelete: PropTypes.func,
  handleToggle: PropTypes.func,
  checked: PropTypes.arrayOf(PropTypes.string),
  isSmallScreen: PropTypes.bool,
};

export default CheckoutList;
