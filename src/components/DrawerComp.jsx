/* eslint-disable react/prop-types */
import {
  Drawer,
  IconButton,
  List,
  ListItemButton,
  Typography,
} from "@mui/material";
import { useState } from "react";
import MenuIcon from "@mui/icons-material/Menu";
import { Link } from "react-router-dom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
import CustomButton from "./Button";
import PropTypes from "prop-types";
import useLogin from "../hooks/useLogin";

const DrawerComp = ({ isLogin }) => {
  const [openDrawer, setOpenDrawer] = useState(false);
  const { handleLogout, role } = useLogin();

  return isLogin === true ? (
    <>
      <Drawer open={openDrawer} onClose={() => setOpenDrawer(false)}>
        <List
          sx={{
            py: 2,
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <ListItemButton>
            <Link to="/checkout" onClick={() => setOpenDrawer(false)}>
              <ShoppingCartIcon style={{ color: "#226957" }} />
            </Link>
          </ListItemButton>
          <ListItemButton>
            <Link
              to="/my-class"
              style={{ textDecoration: "none" }}
              onClick={() => setOpenDrawer(false)}
            >
              <Typography
                variant="subtitle1"
                color="#226957"
                sx={{ textAlign: "justify" }}
              >
                My Class
              </Typography>
            </Link>
          </ListItemButton>
          <ListItemButton>
            <Link
              to="/invoice"
              style={{ textDecoration: "none" }}
              onClick={() => setOpenDrawer(false)}
            >
              <Typography
                variant="subtitle1"
                color="#226957"
                sx={{ textAlign: "justify" }}
              >
                Invoice
              </Typography>
            </Link>
          </ListItemButton>
          {role === "admin" && (
            <ListItemButton>
              <Link
                to="/admin"
                style={{ textDecoration: "none" }}
                onClick={() => setOpenDrawer(false)}
              >
                <PersonIcon style={{ color: "#226957" }} />
              </Link>
            </ListItemButton>
          )}
          <ListItemButton>
            <LogoutIcon
              style={{ color: "#eb5757" }}
              onClick={() => {
                handleLogout();
                setOpenDrawer(false);
              }}
            />
          </ListItemButton>
        </List>
      </Drawer>
      <IconButton onClick={() => setOpenDrawer(!openDrawer)}>
        <MenuIcon />
      </IconButton>
    </>
  ) : (
    <>
      <Drawer open={openDrawer} onClose={() => setOpenDrawer(false)}>
        <List>
          <ListItemButton>
            <Link to="/register" onClick={() => setOpenDrawer(false)}>
              <CustomButton
                label="Sign Up"
                color="#ea9e1f"
                height="40px"
                width="105px"
              />
            </Link>
          </ListItemButton>
          <ListItemButton>
            <Link to="/login" onClick={() => setOpenDrawer(false)}>
              <CustomButton
                label="Login"
                color="#226957"
                height="40px"
                width="86px"
              />
            </Link>
          </ListItemButton>
        </List>
      </Drawer>
      <IconButton onClick={() => setOpenDrawer(!openDrawer)}>
        <MenuIcon />
      </IconButton>
    </>
  );
};

DrawerComp.propTypes = {
  isLogin: PropTypes.bool,
};

export default DrawerComp;
