/* eslint-disable react/prop-types */
import {
  Typography,
  Grid,
  Card,
  CardContent,
  CardMedia,
  Stack,
  CircularProgress,
  CardActionArea,
} from "@mui/material";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { API_URL } from "../constants";
import PropTypes from "prop-types";
import { currencyConvertion } from "../helper/currencyConvertion";
import useLogin from "../hooks/useLogin";

const CourseList = ({ courseData, setShowAlertUnauthorized }) => {
  const { isLogin } = useLogin();
  const navigate = useNavigate();

  const handleGoToDetail = (id) => {
    if (!isLogin) {
      setShowAlertUnauthorized(true);
      return;
    }
    navigate(`/detail-course/${id}`);
  };

  return courseData.length === 0 ? (
    <Stack sx={{ alignItems: "center", my: 5 }}>
      <CircularProgress />
    </Stack>
  ) : (
    <Grid container spacing={4}>
      {courseData.map((data) => (
        <Grid key={data.id} item xs={12} sm={6} lg={4}>
          <Card
            sx={{
              borderRadius: 5,
              cursor: "pointer",
              border: 1,
              borderColor: "grey.500",
              boxShadow: "none",
            }}
            className="cardHovered"
          >
            <CardActionArea onClick={() => handleGoToDetail(data.id)}>
              <CardMedia
                sx={{ height: 260 }}
                image={`${API_URL}${data.image}`}
                title="img"
              />
              <CardContent>
                <Stack minHeight={150} justifyContent={"space-between"}>
                  <div>
                    <Typography
                      color="#BDBDBD"
                      gutterBottom
                      variant="subtitle1"
                      component="div"
                      sx={{ mb: -1 }}
                    >
                      {data.categoryName}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h6"
                      component="div"
                      sx={{ mb: 5, fontWeight: 700 }}
                    >
                      {data.name}
                    </Typography>
                  </div>
                  <Typography
                    variant="h6"
                    color="#226957"
                    sx={{ fontWeight: 700 }}
                  >
                    IDR {currencyConvertion(data.price)}
                  </Typography>
                </Stack>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

CourseList.propTypes = {
  courseData: PropTypes.arrayOf(PropTypes.object),
  setShowAlertUnauthorized: PropTypes.func,
};

export default CourseList;
