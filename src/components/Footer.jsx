/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Grid, Box, Typography, Avatar } from "@mui/material";
import PhoneIcon from "../assets/phoneLogo.svg";
import InstagramIcon from "../assets/instagramLogo.svg";
import YoutubeIcon from "../assets/youtubeLogo.svg";
import TelegramIcon from "../assets/telegramLogo.svg";
import EmailIcon from "../assets/mailLogo.svg";
import { Link } from "react-router-dom";
import "./style.css";
import useLanguageData from "../hooks/useLanguageData";

export default function Footer() {
  const { dataLanguage } = useLanguageData();

  const listContact = [
    {
      logo: PhoneIcon,
      name: "phone",
      url: "tel:08123456789",
    },
    {
      logo: InstagramIcon,
      name: "instagram",
      url: "https://www.instagram.com/",
    },
    {
      logo: YoutubeIcon,
      name: "youtube",
      url: "https://www.youtube.com/",
    },
    {
      logo: TelegramIcon,
      name: "telegram",
      url: "https://web.telegram.org/",
    },
    {
      logo: EmailIcon,
      name: "email",
      url: "mailto:info.dlanguage@gmail.com",
    },
  ];
  return (
    <Box
      sx={{ px: { xs: 4, sm: 6, md: 10 }, py: 6, backgroundColor: "#226957" }}
    >
      <Grid container spacing={12}>
        <Grid item xs={12} md={7} lg={5}>
          <Typography
            sx={{ mb: 1 }}
            variant="h6"
            component="div"
            textAlign="justify"
            color="white"
          >
            About Us
          </Typography>
          <Typography
            variant="subtitle1"
            component="div"
            textAlign="justify"
            color="white"
          >
            Explore our language learning platform! We&apos;re passionate about
            accessible education. Join us on a linguistic adventure, where we
            make language learning fun and enriching. Discover the joy of
            connecting through language. Welcome to our community!
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8} md={5} lg={3}>
          <Typography
            sx={{ mb: 1 }}
            variant="h6"
            component="div"
            textAlign="justify"
            color="white"
          >
            Product
          </Typography>

          <Grid container>
            {dataLanguage.map((data, index) => (
              <Grid item xs={6} key={index}>
                <Link
                  to={`/list-menu-class/${data.id}`}
                  style={{ textDecoration: "none" }}
                >
                  <li
                    style={{ color: "white", marginBottom: "2px" }}
                    className="productList"
                  >
                    {data.categoryName}
                  </li>
                </Link>
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid item xs={12} md={12} lg={4}>
          <Typography
            sx={{ mb: 1 }}
            variant="h6"
            component="div"
            textAlign="justify"
            color="white"
          >
            Address
          </Typography>
          <Typography
            variant="subtitle1"
            component="div"
            textAlign="justify"
            color="white"
          >
            123 Language Street,
            <br />
            Central Jakarta,
            <br />
            Indonesia.
            <br />
          </Typography>
          <Typography
            variant="h6"
            component="div"
            textAlign="justify"
            color="white"
            sx={{ mt: 3, mb: 1 }}
          >
            Contact
          </Typography>
          <Grid container direction={"row"} spacing={2}>
            {listContact.map((Icon, index) => (
              <Grid item key={index}>
                <Avatar
                  onClick={() => window.open(Icon.url, "_blank").focus()}
                  sx={{
                    backgroundColor: "#fff",
                    width: 40,
                    height: 40,
                    cursor: "pointer",
                  }}
                  className="social"
                >
                  <img src={Icon.logo} alt={Icon.name} />
                </Avatar>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}
