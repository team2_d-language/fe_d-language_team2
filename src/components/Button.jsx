/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import Button from "@mui/material/Button";
import PropTypes from "prop-types";

const CustomButton = ({
  logo = null,
  label,
  color,
  height,
  width,
  onClick,
  resize = true,
  disable = false,
  float,
}) => {
  return (
    <Button
      disabled={disable}
      sx={{
        p: 1,
        borderRadius: "8px",
        minWidth: "120px",
        fontSize: "14px",
        lineHeight: "14px",
        "@media (max-width:440px)": {
          maxWidth: resize ? "80px" : "100%",
        },
        "@media (max-width:600px)": {
          minWidth: "110px",
        },
      }}
      variant="contained"
      onClick={onClick}
      style={{
        backgroundColor: color,
        textTransform: "none",
        height: height,
        width: width,
        float: float ? float : "",
      }}
    >
      {logo ? (
        <>
          <img src={logo} alt={label} style={{ marginRight: "8px" }} />
          {label}
        </>
      ) : (
        label
      )}
    </Button>
  );
};

CustomButton.propTypes = {
  logo: PropTypes.string,
  label: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  height: PropTypes.string,
  width: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  resize: PropTypes.bool,
  disable: PropTypes.bool,
  float: PropTypes.string,
};

export default CustomButton;
