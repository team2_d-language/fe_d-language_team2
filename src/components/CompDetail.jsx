/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import {
  Box,
  CircularProgress,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import CustomButton from "./Button";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { ACCESS_TOKEN, API_URL } from "../constants";
import Cookies from "js-cookie";
import PopupAlert from "./PopupAlert";
import PropTypes from "prop-types";
import { formatDate } from "../helper/dateConvertion";
import PaymentModal from "./PaymentModal";
import usePaymentData from "../hooks/usePaymentData";
import { currencyConvertion } from "../helper/currencyConvertion";
import { addDays, format, isSunday } from "date-fns";
import useLogin from "../hooks/useLogin";

const CompDetail = ({ detailCourse }) => {
  const token = Cookies.get(ACCESS_TOKEN);
  const { role } = useLogin();
  const dataPayment = usePaymentData(token);
  const [selectedPayment, setSelectedPayment] = useState("0");
  const isSmallScreen = useMediaQuery("(max-width:660px)");
  const navigate = useNavigate();
  const [selected, setSelected] = useState("");
  const [error, setError] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [errorCode, setErrorCode] = useState();
  const [showAlert, setShowAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState(true);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const tomorrow = addDays(new Date(), 1);
  const nextSevenDays = [];

  for (let i = 0; i < 8; i++) {
    const nextDay = addDays(tomorrow, i);
    if (!isSunday(nextDay)) {
      nextSevenDays.push({
        date: format(nextDay, "yyyy-MM-dd"),
      });
    }
  }

  const handleChange = (event) => {
    setSelected(event.target.value);
  };

  const handleOpenModal = () => {
    if (selected === "") {
      setError(true);
      return;
    }
    handleOpen();
  };

  const handlePayNow = async () => {
    try {
      if (selectedPayment === "0") {
        setErrorMessage(true);
        setAlertMessage("Please select payment method");
        setShowAlert(true);
        return;
      }

      await axios.post(
        `${API_URL}invoice/now`,
        {
          paymentId: selectedPayment,
          courseId: detailCourse.id,
          schedule: selected,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      navigate("/success");
    } catch (e) {
      console.log(e);
      setErrorMessage(true);
      setAlertMessage(e.response.data);
      setShowAlert(true);
    }
  };

  useEffect(() => {
    if (selected) setError(false);
  }, [selected]);

  const handleAddToCart = () => {
    if (selected === "") {
      setError(true);
      return;
    }

    const addToCart = async () => {
      try {
        const response = await axios.post(
          `${API_URL}carts`,
          {
            courseId: detailCourse.id,
            schedule: selected,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        if (response.data.code === 201) {
          setShowAlert(true);
          setErrorMessage(false);
          setErrorCode(200);
          setAlertMessage(response.data.message);
        }
      } catch (error) {
        setErrorCode(error.response.data.code);
        setAlertMessage(error.response.data.message);
        setShowAlert(true);
      }
    };
    addToCart();
  };

  return detailCourse.length == 0 ? (
    <Stack sx={{ alignItems: "center", my: 5, mt: 16 }}>
      <CircularProgress />
    </Stack>
  ) : (
    <>
      <PopupAlert
        message={alertMessage}
        showAlert={showAlert}
        severity={errorCode === 400 || errorMessage ? "error" : "success"}
        onClose={() => {
          setShowAlert(false);
          setErrorCode(200);
        }}
      />
      <Grid
        container
        spacing={4}
        sx={{ pt: "100px", px: isSmallScreen ? 1 : 4 }}
      >
        <Grid item xs={12} sm={6} md={4}>
          <img
            src={`${API_URL}${detailCourse.image}`}
            width={"100%"}
            height={"300px"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={8}>
          <Box>
            <Typography color="#828282" fontSize={18} sx={{ mb: -1 }}>
              {detailCourse.categoryName}
            </Typography>
            <Typography fontSize={24} fontWeight={600} marginY={1}>
              {detailCourse.name}
            </Typography>
            <Typography color="#ea9e1f" fontSize={24} fontWeight={600}>
              IDR {currencyConvertion(detailCourse.price)}
            </Typography>
          </Box>
          {error ? (
            <FormControl
              fullWidth
              error
              sx={{ minWidth: 280, maxWidth: 400, mt: 4, mb: 8 }}
            >
              <InputLabel id="demo-simple-select-label">
                Select Schedule
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selected}
                label="Select Schedule"
                onChange={handleChange}
                disabled={role === "admin"}
              >
                {nextSevenDays.map((data, i) => (
                  <MenuItem key={i} value={data.date}>
                    {formatDate(data.date)}
                  </MenuItem>
                ))}
              </Select>
              <FormHelperText>Please select the date</FormHelperText>
            </FormControl>
          ) : (
            <FormControl
              fullWidth
              sx={{ minWidth: 280, maxWidth: 400, mt: 4, mb: 8 }}
            >
              <InputLabel id="demo-simple-select-label">
                Select Schedule
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selected}
                label="Select Schedule"
                onChange={handleChange}
                disabled={role === "admin"}
              >
                {nextSevenDays.map((data, i) => (
                  <MenuItem key={i} value={data.date}>
                    {formatDate(data.date)}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
          <Stack
            display={"flex"}
            flexDirection={"row"}
            sx={{ mt: "5px", gap: 2 }}
          >
            <CustomButton
              label="Add to Cart"
              color={role === "admin" ? "#ddd" : "#ea9e1f"}
              height="40px"
              width="234px"
              resize={false}
              onClick={handleAddToCart}
              disable={role === "admin"}
            />
            <CustomButton
              label="Buy Now"
              color={role === "admin" ? "#ddd" : "#226957"}
              height="40px"
              width="234px"
              resize={false}
              onClick={handleOpenModal}
              disable={role === "admin"}
            />
          </Stack>
        </Grid>
      </Grid>
      <Box component="section" sx={{ mt: 3, mb: 8, px: isSmallScreen ? 1 : 4 }}>
        <Typography
          color="#333333"
          fontSize={"24px"}
          fontWeight={600}
          marginY={1}
        >
          Deskripsi
        </Typography>
        <Typography color="#333333" fontSize={"16px"} fontWeight={400}>
          {detailCourse.description}
        </Typography>
      </Box>

      <PaymentModal
        open={open}
        handleClose={handleClose}
        handlePayNow={handlePayNow}
        dataPayment={dataPayment}
        selectedPayment={selectedPayment}
        setSelectedPayment={setSelectedPayment}
      />
    </>
  );
};

CompDetail.propTypes = {
  detailCourse: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default CompDetail;
