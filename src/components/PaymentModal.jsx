/* eslint-disable react/prop-types */
import {
  Modal,
  Container,
  Typography,
  List,
  ListItem,
  ListItemText,
  Stack,
} from "@mui/material";
import CustomButton from "../components/Button";
import "./style.css";
import { API_URL } from "../constants";
import PropTypes from "prop-types";

const PaymentModal = ({
  open,
  handleClose,
  handlePayNow,
  dataPayment,
  selectedPayment,
  setSelectedPayment,
}) => {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="parent-modal-title"
      aria-describedby="parent-modal-description"
      sx={{ mt: 8, px: 3 }}
    >
      <Container
        maxWidth="xs"
        sx={{
          backgroundColor: "white",
          borderRadius: "8px",
          py: 3,
          px: 1,
        }}
      >
        <Typography
          id="parent-modal-title"
          textAlign="center"
          fontWeight={500}
          variant="h5"
        >
          Select Payment Method
        </Typography>
        <List
          sx={{
            width: "100%",
            bgcolor: "background.paper",
            mt: 2,
            mb: 3,
          }}
        >
          {dataPayment.map((data) => (
            <ListItem
              key={data.id}
              onClick={() => {
                setSelectedPayment(data.id);
              }}
              sx={{
                borderRadius: "8px",
                backgroundColor: selectedPayment === data.id ? "#ddd" : "white",
              }}
              className="listItem"
            >
              <img src={`${API_URL}${data.logo}`} alt={data.paymentMethod} />
              <ListItemText
                primary={data.paymentMethod}
                primaryTypographyProps={{ fontSize: "20px" }}
                sx={{ ml: 2 }}
              />
            </ListItem>
          ))}
        </List>
        <Stack direction="row" spacing={1} justifyContent="space-between">
          <div style={{ width: "50%" }}>
            <CustomButton
              label="Cancel"
              color="#ea9e1f"
              height="40px"
              width="100%"
              onClick={handleClose}
              resize={false}
            />
          </div>
          <div style={{ width: "50%" }}>
            <CustomButton
              label="Pay Now"
              color={selectedPayment == "0" ? "#ddd" : "#226957"}
              height="40px"
              width="100%"
              onClick={handlePayNow}
              resize={false}
              disable={selectedPayment == "0" ? true : false}
            />
          </div>
        </Stack>
      </Container>
    </Modal>
  );
};

PaymentModal.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  handlePayNow: PropTypes.func,
  dataPayment: PropTypes.arrayOf(PropTypes.object),
  selectedPayment: PropTypes.string,
  setSelectedPayment: PropTypes.func,
};

export default PaymentModal;
