/* eslint-disable react/prop-types */
import { Snackbar, Alert } from "@mui/material";
import PropTypes from "prop-types";

const PopupAlert = ({ message, showAlert, onClose, severity = "success" }) => {
  return (
    <Snackbar
      open={showAlert}
      autoHideDuration={4000}
      onClose={onClose}
      anchorOrigin={{ vertical: "top", horizontal: "left" }}
    >
      <Alert severity={severity}>{message}</Alert>
    </Snackbar>
  );
};

PopupAlert.propTypes = {
  message: PropTypes.string,
  showAlert: PropTypes.bool,
  onClose: PropTypes.func,
  severity: PropTypes.string,
};

export default PopupAlert;
