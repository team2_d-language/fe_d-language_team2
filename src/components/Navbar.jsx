/* eslint-disable no-unused-vars */
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  Stack,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import CustomButton from "./Button";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
import logo from "../assets/logo.png";
import { Link } from "react-router-dom";
import "./style.css";
import DrawerComp from "./DrawerComp";
import useLogin from "../hooks/useLogin";

export default function Navbar() {
  const theme = useTheme();
  const isSmallScreen = useMediaQuery("(max-width:660px)");
  const isMatch = useMediaQuery(theme.breakpoints.down("md"));
  const { handleLogout, isLogin, role } = useLogin();

  return (
    <Box sx={{ flexGrow: 12, mb: 6 }}>
      <AppBar
        elevation={0}
        sx={{ px: isSmallScreen ? 1 : 3, py: 2, backgroundColor: "#ffffff" }}
      >
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <Link to="/" style={{ textDecoration: "none" }}>
            <Typography
              variant="h6"
              component="div"
              sx={{
                display: "flex",
                alignItems: "center",
                fontFamily: "montserrat, sans-serif",
                color: "#000",
              }}
            >
              <img src={logo} className="logo" alt="logo" />
              Language
            </Typography>
          </Link>
          {isLogin ? (
            isMatch ? (
              <>
                <DrawerComp isLogin={true} />
              </>
            ) : (
              <Stack direction="row" spacing={5}>
                <Link to="/checkout">
                  <ShoppingCartIcon
                    style={{ color: "#226957" }}
                    className="navLink"
                  />
                </Link>
                <Link to="/my-class" style={{ textDecoration: "none" }}>
                  <Typography
                    variant="subtitle1"
                    color="#226957"
                    sx={{ textAlign: "justify" }}
                    fontWeight={500}
                    className="navLink"
                  >
                    My Class
                  </Typography>
                </Link>
                <Link to="/invoice" style={{ textDecoration: "none" }}>
                  <Typography
                    variant="subtitle1"
                    color="#226957"
                    sx={{ textAlign: "justify" }}
                    fontWeight={500}
                    className="navLink"
                  >
                    Invoice
                  </Typography>
                </Link>
                <Typography
                  variant="subtitle1"
                  color="#000"
                  sx={{ textAlign: "justify", userSelect: "none" }}
                >
                  |
                </Typography>
                {role === "admin" && (
                  <Link to="/admin">
                    <PersonIcon
                      className="navLink"
                      style={{ color: "#226957" }}
                    />
                  </Link>
                )}
                <LogoutIcon
                  className="navLink"
                  style={{ color: "#eb5757", cursor: "pointer" }}
                  onClick={handleLogout}
                />
              </Stack>
            )
          ) : isMatch ? (
            <>
              <DrawerComp isLogin={false} />
            </>
          ) : (
            <Stack direction={"row"} spacing={1}>
              <Link to="/register">
                <CustomButton
                  label="Sign Up"
                  color="#ea9e1f"
                  height="40px"
                  width="105px"
                />
              </Link>
              <Link to="/login">
                <CustomButton
                  label="Login"
                  color="#226957"
                  height="40px"
                  width="86px"
                />
              </Link>
            </Stack>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
}
