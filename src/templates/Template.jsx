/* eslint-disable react/prop-types */
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import ScrollToTop from "../components/ScrollToTop";

export const NavWithFoot = ({ children }) => {
  return (
    <>
      <ScrollToTop>
        <Navbar />
        {children}
        <Footer />
      </ScrollToTop>
    </>
  );
};

export const NavWithoutFoot = ({ children }) => {
  return (
    <>
      <ScrollToTop>
        <Navbar />
        {children}
      </ScrollToTop>
    </>
  );
};
