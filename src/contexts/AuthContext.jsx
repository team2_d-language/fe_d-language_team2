/* eslint-disable react/prop-types */
import Cookies from "js-cookie";
import { createContext, useEffect, useState } from "react";
import {
  ACCESS_TOKEN,
  API_URL,
  ROLE,
  SUCCESS_LOGIN,
  SUCCESS_LOGOUT,
} from "../constants";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const AuthContext = createContext();

export default function AuthProvider({ children }) {
  const token = Cookies.get(ACCESS_TOKEN);
  const role = Cookies.get(ROLE);
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginError, setLoginError] = useState("");
  const [showAlertLoginFail, setShowAlertLoginFail] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setIsLogin(!!token);
  }, [token]);

  const login = async () => {
    try {
      setLoading(true);
      const response = await axios.post(`${API_URL}login`, {
        email: email,
        password: password,
      });

      const token = response.data.token;
      const role = response.data.role;
      var date = new Date();
      date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
      const expires = "; expires=" + date.toUTCString();
      document.cookie = ACCESS_TOKEN + "=" + token + expires + "; path=/";
      document.cookie = ROLE + "=" + role + expires + "; path=/";
      document.cookie = SUCCESS_LOGIN + "=" + 1 + expires + "; path=/";
      // Cookies.set(ACCESS_TOKEN, token, { expires: 1, secure: true });
      // Cookies.set(ROLE, role, { expires: 1, secure: true });
      // Cookies.set(SUCCESS_LOGIN, 1, { expires: 1, secure: true });
      setLoading(false);
      setEmail("");
      setPassword("");
      navigate("/");
    } catch (error) {
      setLoading(false);
      setLoginError(error.response.data.message);
      setShowAlertLoginFail(true);
    }
  };

  const handleLogout = () => {
    Cookies.set(SUCCESS_LOGOUT, 1, { expires: 1, secure: true });
    Cookies.remove(ACCESS_TOKEN);
    Cookies.remove(ROLE);
    navigate("/login");
  };

  return (
    <AuthContext.Provider
      value={{
        token,
        role,
        isLogin,
        login,
        loading,
        handleLogout,
        navigate,
        email,
        setEmail,
        password,
        setPassword,
        loginError,
        setLoginError,
        showAlertLoginFail,
        setShowAlertLoginFail,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
