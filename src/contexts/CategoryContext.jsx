/* eslint-disable react/prop-types */
import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { API_URL } from "../constants";

export const CategoryContext = createContext();

export default function CategoryProvider({ children }) {
  const [dataLanguage, setDataLanguage] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const responseLanguage = await axios.get(`${API_URL}categories/active`);
        setDataLanguage(responseLanguage.data.categories);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);
  return (
    <CategoryContext.Provider
      value={{
        dataLanguage,
        setDataLanguage,
      }}
    >
      {children}
    </CategoryContext.Provider>
  );
}
