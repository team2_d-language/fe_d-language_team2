/* eslint-disable react/prop-types */
import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { API_URL } from "../constants";

export const CourseContext = createContext();

export default function CourseProvider({ children }) {
  const [dataCourse, setDataCourse] = useState([]);
  useEffect(() => {
    const fetchCourseData = async () => {
      try {
        const response = await axios.get(`${API_URL}courses/active`);
        setDataCourse(response.data.courses);
      } catch (error) {
        console.error("Error fetching course data:", error);
      }
    };

    fetchCourseData();
  }, []);

  return (
    <CourseContext.Provider
      value={{
        dataCourse,
        setDataCourse,
      }}
    >
      {children}
    </CourseContext.Provider>
  );
}
